#pragma once
#include "Base.h"


_BEGIN(Engine)

class CEffect;
class CEffect_Manager final : public CBase
{
	_DECLARE_SINGLETON(CEffect_Manager)
public:
	explicit CEffect_Manager();
	virtual ~CEffect_Manager() = default;	
public:
	HRESULT Add_Prototype_GameEffect(const _tchar* pGameObjectTag, CEffect* pGameObject);
	HRESULT Add_GameEffectToLayer(const _tchar* pProtoTag, const _uint& iSceneID, const _tchar* pLayerTag, CEffect** ppCloneEffect = nullptr);
protected:
	virtual void Free();
};