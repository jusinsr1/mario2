#pragma once
#include "Base.h"

_BEGIN(Engine)

class CTimer;
class CTimer_Manager final : public CBase
{
	_DECLARE_SINGLETON(CTimer_Manager)
public:
	explicit CTimer_Manager();
	virtual ~CTimer_Manager() = default;

public:
	HRESULT Add_Timer(const _tchar* pTimerTag);
	_float Get_TimeDelta(const _tchar* pTimerTag);
private:
	map<const _tchar*, CTimer*>				m_mapTimers;
	typedef map<const _tchar*, CTimer*>		MAPTIMERS;
private:
	CTimer* Find_Timer(const _tchar* pTimerTag);
public:
	virtual void Free() override;
};

_END