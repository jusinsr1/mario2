#pragma once

#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_CarTex final : public CVIBuffer
{
private:
	explicit CBuffer_CarTex(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_CarTex(const CBuffer_CarTex& rhs);
	virtual ~CBuffer_CarTex() = default;
public:
	HRESULT Ready_VIBuffer();
	void	Render_VIBuffer();
public:
	static CBuffer_CarTex* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();


};

_END