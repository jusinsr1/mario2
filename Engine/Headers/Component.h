#pragma once
#include "Base.h"

_BEGIN(Engine)

class _ENGINE_DLL CComponent abstract : public CBase
{
protected:
	explicit CComponent(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CComponent(const CComponent& rhs);
	virtual ~CComponent() = default;
public:
	HRESULT Ready_Component();
protected:
	LPDIRECT3DDEVICE9		m_pGraphic_Device = nullptr;
public:
	virtual CComponent* Clone_Component() = 0;
protected:
	virtual void Free();
};

_END
