#pragma once
#include "Component.h"

_BEGIN(Engine)
class CTransform;
class CGameObject;
class _ENGINE_DLL CCollision final : public CComponent
{
public:
	enum COLGROUP {COL_PLAYER,COL_LAST
		,COL_BUILDING,COL_LASTBUILDING,COL_NORMAL,COL_BOX,COL_TIRE
		,COL_FLAT,COL_MON,COL_SPRING,COL_AXE, COL_COIN,COL_FIRE,COL_END};
private:
	explicit CCollision(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CCollision() = default;

public:
	HRESULT Ready_Collision();
	HRESULT Add_CollGroup(COLGROUP eGroup, CGameObject* pGameObject);
	HRESULT Coll_CollGroup();
public:
	HRESULT	Erase_Object(CGameObject* pObject);
	HRESULT Release_Coll(CGameObject* pObject);

private:
	list<CGameObject*>	m_CollList[COL_END];
	typedef list<CGameObject*>	OBJECTLIST;

private:
	void Coll_Player_Normal();
	void Coll_Player_Tire();
	void Coll_Player_Box();

	void Coll_Player_Spring();
	void Coll_Player_Flat();
	void Coll_Player_Mon();
	void Coll_Player_Coin();


	void Coll_Monster_Normal();
	void Coll_Monster_Box();
	void Coll_Monster_Monster();

	void Coll_Box_Normal();
	void Coll_Box_Building();

	void Coll_Fire_Normal();
	void Coll_Fire_Box();
	void Coll_Fire_Mon();

	void Coll_Last_Axe();

	void Coll_Pang_LastBuilding();
	void Coll_Pang_Normal();

	//void Coll_


//	void Coll_Mon_Bullet();

//	void Coll_Event(COLGROUP eDst);

private:
	//_bool Rect_Cube(CTransform*& left, CTransform*& right);

private:
	_bool Coll_Rect1(const RC& rc1, const RC& rc2);
	_bool Coll_Rect2(const RC& rc1, const RC& rc2,_vec3* vRap);
	_bool Coll_Rect3(const RC& rc1, const RC& rc2, _vec3* vRap);

	_bool Coll_Cube(const CB& cb1, const CB& cb2, _vec3* vRap);
public:
	static CCollision* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();
};

_END