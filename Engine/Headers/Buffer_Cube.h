#pragma once
#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_Cube final: public CVIBuffer
{
private:
	explicit CBuffer_Cube(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_Cube(const CBuffer_Cube& rhs);
	virtual ~CBuffer_Cube() = default;

public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer();
public:
	static CBuffer_Cube* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();
};
_END
