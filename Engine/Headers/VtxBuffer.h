#pragma once

#include "Component.h"

_BEGIN(Engine)

class _ENGINE_DLL CVtxBuffer :
	public CComponent
{
protected:
	explicit CVtxBuffer(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CVtxBuffer(const CVtxBuffer& rhs);
	virtual ~CVtxBuffer() = default;
public:
	HRESULT	Ready_CVtxBuffer(const _uint iNumVertices);

protected:
	_uint	m_iNumVertices;
	_ulong	m_dwFVF = 0;
protected:
	LPDIRECT3DVERTEXBUFFER9 m_pVB = nullptr;
public:
	virtual CComponent* Clone_Component() = 0;
protected:
	virtual void Free();
};

_END