#pragma once
#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_Logo :
	public CVIBuffer
{
private:
	explicit CBuffer_Logo(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_Logo(const CBuffer_Logo& rhs);
	virtual ~CBuffer_Logo() = default;
public:
	HRESULT			Ready_VIBuffer();
	void			Render_VIBuffer();
public:
	static CBuffer_Logo* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();

};

_END