#include "..\Headers\Buffer_CubeLine.h"
#include "Transform.h"

CBuffer_CubeLine::CBuffer_CubeLine(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVIBuffer(pGraphic_Device)
{

}

CBuffer_CubeLine::CBuffer_CubeLine(const CBuffer_CubeLine & rhs)
	: CVIBuffer(rhs)
{

}

HRESULT CBuffer_CubeLine::Ready_VIBuffer()
{
	m_iNumVertices = 36;
	m_iStride = sizeof(VTXCUBENMCOLTEX);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE |D3DFVF_TEX1| D3DFVF_TEXCOORDSIZE3(0);

	m_iNumPolygons = 12;
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;
	//////////////////////////////////////////////////////////////////////
	m_pPosition = new _vec3[m_iNumVertices];
	
	VertexSet();
	//m_pIndex = new POLYGON16[m_iNumPolygons];

	/////////////////////////////////////////////////////////////////////
	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;


	VTXCUBENMCOLTEX*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);


	pVertices[0].vPosition = m_pPosition[0];
	pVertices[1].vPosition = m_pPosition[1];
	pVertices[2].vPosition = m_pPosition[2];
	pVertices[3].vPosition = m_pPosition[0];
	pVertices[4].vPosition = m_pPosition[2];
	pVertices[5].vPosition = m_pPosition[3];

	pVertices[6].vPosition = m_pPosition[4];
	pVertices[7].vPosition = m_pPosition[0];
	pVertices[8].vPosition = m_pPosition[3];
	pVertices[9].vPosition = m_pPosition[4];
	pVertices[10].vPosition = m_pPosition[3];
	pVertices[11].vPosition = m_pPosition[7];
	
	pVertices[12].vPosition = m_pPosition[5];
	pVertices[13].vPosition = m_pPosition[4];
	pVertices[14].vPosition = m_pPosition[7];
	pVertices[15].vPosition = m_pPosition[5];
	pVertices[16].vPosition = m_pPosition[7];
	pVertices[17].vPosition = m_pPosition[6];

	pVertices[18].vPosition = m_pPosition[1];
	pVertices[19].vPosition = m_pPosition[5];
	pVertices[20].vPosition = m_pPosition[6];
	pVertices[21].vPosition = m_pPosition[1];
	pVertices[22].vPosition = m_pPosition[6];
	pVertices[23].vPosition = m_pPosition[2];

	pVertices[24].vPosition = m_pPosition[4];
	pVertices[25].vPosition = m_pPosition[5];
	pVertices[26].vPosition = m_pPosition[1];
	pVertices[27].vPosition = m_pPosition[4];
	pVertices[28].vPosition = m_pPosition[1];
	pVertices[29].vPosition = m_pPosition[0];

	pVertices[30].vPosition = m_pPosition[3];
	pVertices[31].vPosition = m_pPosition[2];
	pVertices[32].vPosition = m_pPosition[6];
	pVertices[33].vPosition = m_pPosition[3];
	pVertices[34].vPosition = m_pPosition[6];
	pVertices[35].vPosition = m_pPosition[7];


	for (size_t i = 0; i < m_iNumVertices; ++i)
	{
		pVertices[i].vTexUV = pVertices[i].vPosition; 
		pVertices[i].dwColor = D3DXCOLOR(0.f, 0.f, 0.f, 1.f);
		pVertices[i].vNormal = _vec3(0.f, 0.f, 0.f);
	}

	m_pVB->Unlock();



	LINE16*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	pIndices[0]._0 = 0;		pIndices[0]._1 = 1;
	pIndices[1]._0 = 2;		pIndices[1]._1 = 5; 
	pIndices[2]._0 = 0;		pIndices[2]._1 = 6;
	pIndices[3]._0 = 12;	pIndices[3]._1 = 17; 
	pIndices[4]._0 = 11;	pIndices[4]._1 = 6; 
	pIndices[5]._0 = 12;	pIndices[5]._1 = 1;
	pIndices[6]._0 = 2;		pIndices[6]._1 = 17; 
	pIndices[7]._0 = 11;	pIndices[7]._1 = 5;

	m_pIB->Unlock();



	UpdateNormal();


	return NOERROR;
}

void CBuffer_CubeLine::Render_VIBuffer()
{
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);

	
	//m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);

	m_pGraphic_Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, m_iNumPolygons);
	m_pGraphic_Device->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_COLOR1);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_LINESTRIP, 0,0, m_iNumVertices,0, 15);
	m_pGraphic_Device->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
	//m_pGraphic_Device->DrawPrimitiveUP(D3DPT_LINELIST, 4,m_pPosition,sizeof(VTXCUBENMTEX));
}

void CBuffer_CubeLine::VertexSet()
{
	m_pPosition[0] = _vec3(-0.5f, 0.5f, -0.5f);
	m_pPosition[1] = _vec3(0.5f, 0.5f, -0.5f);
	m_pPosition[2] = _vec3(0.5f, -0.5f, -0.5f);
	m_pPosition[3] = _vec3(-0.5f, -0.5f, -0.5f);
	m_pPosition[4] = _vec3(-0.5f, 0.5f, 0.5f);
	m_pPosition[5] = _vec3(0.5f, 0.5f, 0.5f);
	m_pPosition[6] = _vec3(0.5f, -0.5f, 0.5f);
	m_pPosition[7] = _vec3(-0.5f, -0.5f, 0.5f);
}




CBuffer_CubeLine * CBuffer_CubeLine::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_CubeLine*	pInstance = new CBuffer_CubeLine(pGraphic_Device);

	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		MessageBox(0, L"CBuffer_CubeTex Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}


CComponent * CBuffer_CubeLine::Clone_Component()
{
	return new CBuffer_CubeLine(*this);
}

void CBuffer_CubeLine::Free()
{
	Safe_Delete_Array(m_pPosition);

	CVIBuffer::Free();
}
