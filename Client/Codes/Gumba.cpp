#include "stdafx.h"
#include "Gumba.h"
#include "Management.h"
#include "Buffer_Player.h"
#include "ObjMove.h"
#include "Collision.h"
#include "Plane.h"

_USING(Client)
CGumba::CGumba(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{

}

CGumba::CGumba(const CGumba & rhs)
	:CGameObject(rhs)
{
	
}

HRESULT CGumba::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CGumba::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_MON, this)))
		return -1;



	return NOERROR;
}

_int CGumba::Update_GameObject(const _float & fTImeDelta)
{
	
	if (eState == GUMBA_DEAD)
	{
		if (!m_bIsSound)
		{
			m_bIsSound = true;
			CSound_Manager::GetInstance()->PlaySoundW(L"Kick.wav", CSound_Manager::EFFECT);
		}

		ySpd -= 0.12f;
		m_pTransformCom->MoveV3({ 0.f,ySpd,0.f });
		fDeadDelay += fTImeDelta;
		if (fDeadDelay >= 1.f)	
			return 1;

		return 0;
	}

	AsGravity(fTImeDelta);//중력에의한움직임

	if (curPlane == -1)
		curPlane = prePlane;

	m_pObjMove->Setup_Obj(m_pPlane->Find_PlaneInfo(curPlane), m_pTransformCom,prePlane);//값셋팅

	m_pObjMove->Update();//업데이트

	if (m_pObjMove->CheckIn(curPlane, prePlane))//평면이 바뀌진않았는지 체크
	{
		bCheck = true;
		moveCheck();		 //면이바뀜에따른 속도방향전환
	}
	m_pObjMove->Get_ClimbDir(climbDir);//올라온 벽 전해받기
	m_pObjMove->Get_TouchDir(touchDir, offX, offY);//터치방향과 오프셋전해받기
	
	Move_Frame(0.5f, fTImeDelta, 2);



	return _int();
}

_int CGumba::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	
	//m_iNumPlane = curPlane;

	return _int();
}

void CGumba::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;

	if (!bCheck)
		m_pBufferCom->SetUp_BufferPlayer(m_pPlane->Find_PlaneInfo(curPlane), touchDir, climbDir, offX, offY);
	else
		bCheck = false;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);


}

CGumba * CGumba::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CGumba* pInstance = new CGumba(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CGumba Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CGumba::Clone_GameObject()
{
	CGumba* pInstance = new CGumba(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CGumba Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

_bool CGumba::GetRC(RC& rc)
{
	if (eState == GUMBA_DEAD)
		return false;

	if (curPlane == -1)
		return false;

	if (rc.plane == 0)//몬스터가 처음
	{

	}
	else//몬스터가 두번z째
	{
		if (rc.plane != curPlane)
			return false;
	}


	rc.plane = curPlane;
	rc.climeDir = climbDir;
	m_pTransformCom->GetRC2D(rc);

	return true;
}

HRESULT CGumba::Ready_Component()
{

	CManagement* pManagement = CManagement::GetInstance();
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_Player*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_Player");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pObjMove = (CObjMove*)pManagement->Clone_Component(SCENE_STAGE, L"Component_ObjMove_Player");
	if (FAILED(Add_Component(L"Com_ObjMove", m_pObjMove)))
		return E_FAIL;

	m_pPlane = (CPlane*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Plane");
	if (FAILED(Add_Component(L"Com_Plane", m_pPlane)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Gumba");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

void CGumba::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation)
{


	m_iNumPlane = iPlaneNum;


	vSpd.x = 3.f;


	curPlane = iPlaneNum;
	prePlane = iPlaneNum;

	climbDir = Direction(iRotation);


	_vec3 vPos = Position;
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.017f, TexSize.y*0.017f, TexSize.z);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);

}


void CGumba::AsGravity(const _float & fTImeDelta)
{
	m_pTransformCom->Move(fTImeDelta, vSpd);


	if (vSpd.y >= -15.f)
	{
		vSpd.y -= 0.5f;
	}

}

void CGumba::moveCheck()
{
	if (curPlane % 5 == 4)
		return;


	if (prePlane % 5 != 4)
		return;


	float fCur = 0.f;

	switch ((curPlane % 5) - climbDir)
	{
	case 1:
	case 3:
		fCur = vSpd.y;
		vSpd.y = vSpd.x;
		vSpd.x = -fCur;
		break;
	case -2:
	case 2:
		vSpd.y *= -1.f;
		break;
	case -1:
	case -3:
		fCur = vSpd.y;
		vSpd.y = -vSpd.x;
		vSpd.x = fCur;
		break;
	}

	//= curPlane % 5;

}

void CGumba::Coll_Move(_vec3& vMove, _int i)
{


	_float vTick = 0.f;

	if (curPlane % 5 == 4)//옥상일때
	{
		switch (climbDir)
		{
		case 0:
			vTick = vMove.z;
			break;
		case 1:
			vTick = vMove.x;
			break;
		case 2:
			vTick = -vMove.z;
			break;
		case 3:
			vTick = -vMove.x;
			break;
		}
	}
	else
	{
		vTick = vMove.y;
	}

	if (i == CCollision::COL_NORMAL|| i == CCollision::COL_BOX)
	{
		m_pTransformCom->MoveV3(vMove);
		if (fabs(vTick) < FLT_EPSILON)
		{
			vSpd.x *= -1.f;
		}
	}
	else if (i == CCollision::COL_PLAYER)
	{
		if (eState == GUMBA_MOVE)
		{

			if (fabs(vTick) > FLT_EPSILON)//위에서밟음
			{
				eState = GUMBA_DEAD;
				m_iTexIdx = 2;
			}


		}
			
	}
	else if (i == CCollision::COL_FIRE)
	{
		eState = GUMBA_DEAD;
	}
	else if (i == CCollision::COL_MON)
	{
		m_pTransformCom->MoveV3(vMove);
		vSpd.x *= -1.f;
	}
	


}



_bool CGumba::IsTurtle()
{
	return false;
}

void CGumba::Free()
{	
	if (m_bIsColne)
	{
		m_pCollCom->Erase_Object(this);
	}
	
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pObjMove);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pPlane);


	CGameObject::Free();
}
