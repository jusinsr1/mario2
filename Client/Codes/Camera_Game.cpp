#include "stdafx.h"
#include "..\Headers\Camera_Game.h"
#include "ObjMove.h"
#include "Player.h"

_USING(Client)

CCamera_Game::CCamera_Game(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CCamera(pGraphic_Device)
{
}

CCamera_Game::CCamera_Game(const CCamera_Game & rhs)
	: CCamera(rhs)
{
}

HRESULT CCamera_Game::Ready_Prototype()
{
	if (FAILED(CCamera::Ready_Prototype()))
		return E_FAIL;

	return NOERROR;
}

HRESULT CCamera_Game::Ready_GameObject()
{
	if (FAILED(CCamera::Ready_GameObject()))
		return E_FAIL;

	m_pTransform->SetUp_Speed(1.f, 1.f);


	return NOERROR;
}

_int CCamera_Game::Update_GameObject(const _float & fTimeDelta)
{
	if (GET_INSTANCE(CInput_Device)->IsKeyDown(DIK_SPACE))
	{
		m_iCameraState = 1;
		m_ProjDesc.fFovY = D3DXToRadian(60);
		SetUp_CameraProjDesc(m_CameraDesc, m_ProjDesc);
	}
	if (GET_INSTANCE(CInput_Device)->IsKeyUp(DIK_SPACE))
	{
		m_iCameraState = 0;
		m_ProjDesc.fFovY = D3DXToRadian(60);
		SetUp_CameraProjDesc(m_CameraDesc, m_ProjDesc);
	}
	if (m_pPlayerMove == nullptr)
		return -1;

	m_PlaneDir = m_pPlayer->Get_PlaneNum() % 5;
	if (m_PlaneDir != 4)
		m_ClimbDir = (Direction)m_PlaneDir;
	if(m_pPlayer->Get_Pos().z >= 346.5f)
		m_iCameraState = 2;

	if (m_iCameraState == 2)
		Ending_Camera(fTimeDelta);
	else if (m_iCameraState == 0)
	{
		Default_Camera();
		CalCamera();
	}
	else if (m_iCameraState == 1)
		Fps_Camera();
	
	Invalidate_ViewProjMatrix();
	return _int();
}

_int CCamera_Game::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CCamera_Game::Render_GameObject()
{
}

void CCamera_Game::Default_Camera()
{
	


	PLANEINFO* pPlane = m_pPlayerMove->Get_pPlane();
	_int NextDir = -1;
	_vec2 PosInPlayer = m_pPlayerMove->Get_Player();
	_vec3 vCameraPos, vLook, vUp, PlaneInPlayer, CameraDist;

	if ((pPlane->fWidth / 2) - PosInPlayer.x< 3)
	{
		if (pPlane->arrMoveablePlane[3] != -1)
			NextDir = pPlane->arrMoveablePlane[3] % 5;
	}
	else if (-(pPlane->fWidth / 2) - PosInPlayer.x > -3)
	{
		if (pPlane->arrMoveablePlane[1] != -1)
			NextDir = pPlane->arrMoveablePlane[1] % 5;
	}
	else if ((pPlane->fHeight / 2) - PosInPlayer.y< 3)
	{
		if (pPlane->arrMoveablePlane[2] != -1)
			NextDir = pPlane->arrMoveablePlane[2] % 5;
	}
	else if (-(pPlane->fHeight / 2) - PosInPlayer.y > -3)
	{
		if (pPlane->arrMoveablePlane[0] != -1)
			NextDir = pPlane->arrMoveablePlane[0] % 5;
	}
	if (m_PlaneDir == 0)
	{
		PlaneInPlayer = { PosInPlayer.x / 2, PosInPlayer.y, 0.f };
		CameraDist = { 0.f, 1.f, -30.f };
	}
	else if (m_PlaneDir == 1)
	{
		PlaneInPlayer = { 0.f, PosInPlayer.y, -PosInPlayer.x/2 };
		CameraDist = { -30.f, 1.f, 0.f };
	}
	else if (m_PlaneDir == 2)
	{
		PlaneInPlayer = { -PosInPlayer.x / 2, PosInPlayer.y, 0.f };
		CameraDist = { 0.f, 1.f, +30.f };
	}
	else if (m_PlaneDir == 3)
	{
		PlaneInPlayer = { 0.f, PosInPlayer.y, PosInPlayer.x / 2 };
		CameraDist = { +30.f, 1.f, 0.f };
	}
	else if (m_PlaneDir == 4)
	{
		PlaneInPlayer = { PosInPlayer.x, -1.f, PosInPlayer.y/2 };
		CameraDist = { +0.f, 30.f, 0.f };
	}

	if (pPlane->iPlaneNum >= 40)
		PlaneInPlayer.z = PlaneInPlayer.z * 2;

	if (NextDir != m_PlaneDir)
	{
		if (NextDir == 0)
			CameraDist += { 0.f, 0.f, -pPlane->fHeight / 2 };
		else if (NextDir == 1)
			CameraDist += { -pPlane->fWidth / 2, 0.f, 0.f };
		else if (NextDir == 2)
			CameraDist += { 0.f, 0.f, +pPlane->fWidth / 2 };
		else if (NextDir == 3)
			CameraDist += { +pPlane->fWidth / 2, 0.f, 0.f };
		else if (NextDir == 4)
			CameraDist += { 0.f, pPlane->fHeight / 2, 0.f };
	}

	vCameraPos = pPlane->Pos + PlaneInPlayer + CameraDist;
	_vec3 Camera = vCameraPos - m_vCurCameraPos;
	_float CameraMove = D3DXVec3Length(&Camera);


	if (CameraMove >= 0.5f)
	{
		D3DXVec3Normalize(&Camera, &Camera);
		Camera = Camera*0.5f;
	}
	m_vCurCameraPos += Camera;
	vLook = pPlane->Pos + PlaneInPlayer - m_vCurCameraPos;

	_vec3 vMoveLook = vLook - m_vCurLook;
	_float LookMove = D3DXVec3Length(&vMoveLook);
	if (LookMove >= 0.5f)
	{
		D3DXVec3Normalize(&vMoveLook, &vMoveLook);
		vMoveLook = vMoveLook*0.5f;
		m_vCurLook += vMoveLook*0.1f;
	}
	else
		m_vCurLook = vLook;


	if (m_PlaneDir == 4)
	{
		if (m_ClimbDir == 0)
			vUp = { 0.f,0.f,1.f };
		else if (m_ClimbDir == 1)
			vUp = { 1.f,0.f,0.f };
		else if (m_ClimbDir == 2)
			vUp = { 0.f,0.f,-1.f };
		else if (m_ClimbDir == 3)
			vUp = { -1.f,0.f,0.f };
	}
	else
		vUp = { 0.f,1.f,0.f };
	_vec3 vMoveUp = vUp - m_vCurUp;
	_float UpMove = D3DXVec3Length(&vMoveUp);
	if (UpMove >= 0.1f)
	{
		D3DXVec3Normalize(&vMoveUp, &vMoveUp);
		vMoveUp = vMoveUp*0.1f;
		m_vCurUp += vMoveUp*0.1f;
	}
	else
		m_vCurUp = vUp;
}

void CCamera_Game::CalCamera()
{
	_vec3 vRight;
	D3DXVec3Normalize(&m_vCurUp, &m_vCurUp);
	D3DXVec3Normalize(&m_vCurLook, &m_vCurLook);
	D3DXVec3Cross(&vRight, &m_vCurUp, &m_vCurLook);
	D3DXVec3Normalize(&vRight, &vRight);

	m_pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransform->Set_StateInfo(CTransform::STATE_UP, &m_vCurUp);
	m_pTransform->Set_StateInfo(CTransform::STATE_LOOK, &m_vCurLook);
	m_pTransform->Set_StateInfo(CTransform::STATE_POSITION, &m_vCurCameraPos);
}

void CCamera_Game::Fps_Camera()
{
	CTransform* PlayerTrans = (CTransform*)m_pPlayer->Get_ComponentPointer(L"Com_Transform");
	_matrix Playermatrix = PlayerTrans->Get_Matrix();
	m_pTransform->Set_Matrix(Playermatrix);
	_int iPlayerDir = (_int)m_pPlayer->Get_Dir();

	_int iDegree;
	m_pTransform->Go_Up(D3DXToRadian(50));
	m_pTransform->BackWard(D3DXToRadian(50));


	if (iPlayerDir == 0)
		iDegree = 240;
	else
		iDegree = 120;


	if (m_PlaneDir == 4)
	{
		if (m_ClimbDir == 0)
			m_pTransform->Rotation_Z(D3DXToRadian(iDegree));
		else if (m_ClimbDir == 1)
			m_pTransform->Rotation_X(D3DXToRadian(iDegree));
		else if (m_ClimbDir == 2)
			m_pTransform->Rotation_Z(D3DXToRadian(-iDegree));
		else if (m_ClimbDir == 3)
			m_pTransform->Rotation_X(D3DXToRadian(-iDegree));
	}
	else
		m_pTransform->Rotation_Y(D3DXToRadian(iDegree));

	m_pTransform->BackWard(D3DXToRadian(50));

}

void CCamera_Game::Ending_Camera(const _float& fTimeDelta)
{
	_vec3 vRight, vUp, vLook, vPosition;
	if (m_fEndingTime >= 6 && CameraChange == false)
	{
		CameraChange = true;
		//������
	}

	if(CameraChange == false)
	{
		vUp = { -0.134936f, 0.917233f, -0.374797f };
		vLook = { -0.310702f ,-0.398347f, -0.863007f };

		D3DXVec3Normalize(&vUp, &vUp);
		D3DXVec3Normalize(&vLook, &vLook);

		D3DXVec3Cross(&vRight, &vUp, &vLook);
		D3DXVec3Normalize(&vRight, &vRight);

		_matrix matrixCamera;
		D3DXMatrixIdentity(&matrixCamera);
		matrixCamera._11 = vRight.x;
		matrixCamera._12 = vRight.y;
		matrixCamera._13 = vRight.z;

		matrixCamera._21 = vUp.x;
		matrixCamera._22 = vUp.y;
		matrixCamera._23 = vUp.z;

		matrixCamera._31 = vLook.x;
		matrixCamera._32 = vLook.y;
		matrixCamera._33 = vLook.z;

		matrixCamera._41 = 383.954f;
		matrixCamera._42 = 161.355f;
		matrixCamera._43 = 343.554f;

		m_pTransform->Set_Matrix(matrixCamera);
	}
	else
	{
		_vec3 PlayerPos = m_pPlayer->Get_Pos();
		vPosition.x = 381.f;
		vPosition.y = 150.f + 22.f;
		vPosition.z = PlayerPos.z;

		vRight = { 0.f,0.f,1.f };
		vLook = { 0.f, -1.f , 0.f };
		vUp = { -1.f, 0.f, 0.f };

		m_pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
		m_pTransform->Set_StateInfo(CTransform::STATE_UP, &vUp);
		m_pTransform->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
		m_pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
	}
	m_fEndingTime += fTimeDelta;
}

CCamera_Game * CCamera_Game::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamera_Game* pInstance = new CCamera_Game(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CCamera_Debug Created Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CCamera_Game::Clone_GameObject()
{
	CCamera_Game* pInstance = new CCamera_Game(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CCamera_Game Clone Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

void CCamera_Game::Free()
{
	CCamera::Free();
}
