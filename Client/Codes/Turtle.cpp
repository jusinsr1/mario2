#include "stdafx.h"
#include "Turtle.h"
#include "Management.h"
#include "Buffer_Player.h"
#include "ObjMove.h"
#include "Collision.h"
#include "Plane.h"

_USING(Client)
CTurtle::CTurtle(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{

}

CTurtle::CTurtle(const CTurtle & rhs)
	: CGameObject(rhs)
{

}

HRESULT CTurtle::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CTurtle::Ready_GameObject()
{
	
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;

	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_MON, this)))
		return -1;



	return NOERROR;
}

_int CTurtle::Update_GameObject(const _float & fTImeDelta)
{
	if (vSpd.x > 0)
		m_eDir = DIR_RIGHT;
	else
		m_eDir = DIR_LEFT;

	if (eState == TURTLE_DEAD)
	{
		ySpd -= 0.12f;
		m_pTransformCom->MoveV3({0.f,ySpd,0.f});

		if (!m_bIsSound[1])
		{
			m_bIsSound[1] = true;
			CSound_Manager::GetInstance()->PlaySoundW(L"Kick.wav", CSound_Manager::EFFECT);
		}

		m_iTexIdx = 2;

		fDeadDelay += fTImeDelta;
		if (fDeadDelay >= 1.f)
			return 1;

		return 0;
	}

	AsGravity(fTImeDelta);//중력에의한움직임

	if (curPlane == -1)
		curPlane = prePlane;



	m_pObjMove->Setup_Obj(m_pPlane->Find_PlaneInfo(curPlane), m_pTransformCom, prePlane);//값셋팅

	m_pObjMove->Update();//업데이트

	if (m_pObjMove->CheckIn(curPlane, prePlane))//평면이 바뀌진않았는지 체크
	{
		bCheck = true;
		moveCheck();		 //면이바뀜에따른 속도방향전환
	}
	m_pObjMove->Get_ClimbDir(climbDir);//올라온 벽 전해받기
	m_pObjMove->Get_TouchDir(touchDir, offX, offY);//터치방향과 오프셋전해받기

	Move_Frame(0.5f, fTImeDelta, 2);


	return _int();
}

_int CTurtle::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;


	return _int();
}

void CTurtle::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom[m_eDir]->SetUp_OnGraphicDev(m_iTexIdx)))
		return;

	if (!bCheck)
		m_pBufferCom->SetUp_BufferPlayer(m_pPlane->Find_PlaneInfo(curPlane), touchDir, climbDir, offX, offY);
	else
		bCheck = false;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}


CTurtle * CTurtle::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CTurtle* pInstance = new CTurtle(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CTurtle Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CTurtle::Clone_GameObject()
{
	CTurtle* pInstance = new CTurtle(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CTurtle Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

_bool CTurtle::GetRC(RC& rc)
{
	if (eState == TURTLE_DEAD)
		return false;

	if (curPlane == -1)
		return false;

	if (rc.plane == 0)//몬스터가 처음
	{

	}
	else//몬스터가 두번째
	{
		if (rc.plane != curPlane)
			return false;
	}


	rc.plane = curPlane;
	rc.climeDir = climbDir;
	m_pTransformCom->GetRC2D(rc);

	return true;
}

HRESULT CTurtle::Ready_Component()
{
	CManagement* pManagement = CManagement::GetInstance();
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_Player*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_Player");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pObjMove = (CObjMove*)pManagement->Clone_Component(SCENE_STAGE, L"Component_ObjMove_Player");
	if (FAILED(Add_Component(L"Com_ObjMove", m_pObjMove)))
		return E_FAIL;

	m_pPlane = (CPlane*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Plane");
	if (FAILED(Add_Component(L"Com_Plane", m_pPlane)))
		return E_FAIL;

	m_pTextureCom[DIR_LEFT] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Turtle_Left");
	if (FAILED(Add_Component(L"Com_Texture_Left", m_pTextureCom[DIR_LEFT])))
		return E_FAIL;

	m_pTextureCom[DIR_RIGHT] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Turtle_Right");
	if (FAILED(Add_Component(L"Com_Texture_Right", m_pTextureCom[DIR_RIGHT])))
		return E_FAIL;


	Safe_Release(pManagement);

	return NOERROR;
}

void CTurtle::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation)
{

	m_iNumPlane = iPlaneNum;


	vSpd.x = 3.f;


	curPlane = iPlaneNum;
	prePlane = iPlaneNum;

	climbDir = Direction(iRotation);


	_vec3 vPos = Position;
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

	_vec3 TexSize = *m_pTextureCom[m_eDir]->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.017f, TexSize.y*0.017f, TexSize.z);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);

}


void CTurtle::AsGravity(const _float & fTImeDelta)
{
	m_pTransformCom->Move(fTImeDelta, vSpd);


	if (vSpd.y >= -15.f)
	{
		vSpd.y -= 0.5f;
	}
}

void CTurtle::moveCheck()
{
	if (curPlane % 5 == 4)
		return;


	if (prePlane % 5 != 4)
		return;


	float fCur = 0.f;

	switch ((curPlane % 5) - climbDir)
	{
	case 1:
	case 3:
		fCur = vSpd.y;
		vSpd.y = vSpd.x;
		vSpd.x = -fCur;
		break;
	case -2:
	case 2:
		vSpd.y *= -1.f;
		break;
	case -1:
	case -3:
		fCur = vSpd.y;
		vSpd.y = -vSpd.x;
		vSpd.x = fCur;
		break;
	}

	//= curPlane % 5;

}

void CTurtle::Coll_Move(_vec3& vMove, _int i)
{
	if (eState == TURTLE_DEAD)
	{
		return;
	}

	m_bIsSound[0] = false;


	_float vTick = 0.f;
	_float fR = 0.f;

	if (curPlane % 5 == 4)//옥상일때
	{
		switch (climbDir)
		{
		case 0:
			vTick = vMove.z;
			fR = vMove.x;
			break;
		case 1:
			vTick = vMove.x;
			fR = -vMove.z;
			break;
		case 2:
			vTick = -vMove.z;
			fR = -vMove.x;
			break;
		case 3:
			vTick = -vMove.x;
			fR = vMove.z;
			break;
		}
	}
	else
	{
		switch (climbDir)
		{
		case 0:
			fR = vMove.x;
			break;
		case 1:
			fR = -vMove.z;
			break;
		case 2:
			fR = -vMove.x;
			break;
		case 3:
			fR = vMove.z;
			break;

		}
		vTick = vMove.y;
	}

	if (i == CCollision::COL_NORMAL|| i == CCollision::COL_BOX)
	{
		m_pTransformCom->MoveV3(vMove);
		if (fabsf(vTick) < FLT_EPSILON)
		{
			

			vSpd.x *= -1.f;
			//여기에 부딛히는소리넣어라

			if (eState == TURTLE_DOWN)
			{
				if (!m_bIsSound[0])
				{
					m_bIsSound[0] = true;
					CSound_Manager::GetInstance()->PlaySoundW(L"Bump.wav", CSound_Manager::EFFECT);
				}
			}
		}
	}
	else if (i == CCollision::COL_PLAYER)
	{
		//m_pTransformCom->MoveV3(vMove);

		if (fabs(vTick) < FLT_EPSILON)//옆에서부딫힘
		{

			if (eState == TURTLE_DOWN)
			{
			
				CSound_Manager::GetInstance()->PlaySoundW(L"Kick.wav", CSound_Manager::EFFECT);

				if (vSpd.x < FLT_EPSILON)//멈춰있다면
				{
					vSpd.x =15.f*( fR/fabsf(fR));
				}
			}

		}
		else//위에서밟음
		{
			eState = TURTLE_DOWN;
			vSpd.x = 0.f;
			CSound_Manager::GetInstance()->PlaySoundW(L"Kick.wav", CSound_Manager::EFFECT);
			
			if (!m_bIsSound[3])
			{
				
				m_bIsSound[3] = true;
			}
		}
		
	}
	else if (i==CCollision::COL_FIRE)
	{
		eState = TURTLE_DEAD;
	}
	else if (i == CCollision::COL_MON)
	{
		m_pTransformCom->MoveV3(vMove);
		vSpd.x *= -1.f;
	}


	if (eState == TURTLE_DOWN)
	{
		m_iTexIdx = 2;
		//CSound_Manager::GetInstance()->PlaySoundW(L"Kick.wav", CSound_Manager::EFFECT);
	}

}

_bool CTurtle::IsTurtle()
{
	if (fabsf(vSpd.x) > FLT_EPSILON)
	{
		if (eState == TURTLE_DOWN)
		{
			return true;
		}

	}
	return false;

}

void CTurtle::Free()
{

	for (size_t i = 0; i < 2; i++)
		Safe_Release(m_pTextureCom[i]);

	if (m_bIsColne)
	{
		m_pCollCom->Erase_Object(this);
	}


	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pObjMove);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pPlane);


	CGameObject::Free();
}

