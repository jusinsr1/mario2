#include "stdafx.h"
#include "Particle.h"

_USING(Client)

CParticle::CParticle(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CEffect(pGraphic_Device)
{
}

CParticle::CParticle(CParticle & rhs)
	:CEffect(rhs)
{
}

HRESULT CParticle::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CParticle::Ready_GameObject(PARTICLE eID , const _int iCurNumPlane)
{	
	ZeroMemory(&m_Bute, sizeof(ATTIBUTE));

	m_eID = eID;

	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (iCurNumPlane == -1)
		m_iNumPlane = 4;
	else
		m_iNumPlane = iCurNumPlane % 5;

	m_pTransformCom->Scaling(1.f, 1.f, 1.f);
	m_pTransformCom->SetUp_Speed(1.f, 1.f);

	return NOERROR;
}

_int CParticle::Update_GameObject(const _float & fTimeDelta)
{
	if (!m_bIsLateReady)
	{
		if (0x80000000 & Ready_State())
			return -1;

		m_pParticleCom->Reset_Attibute(&m_Bute, 5 , 20 , m_iNumPlane , (const _uint)m_eID);

		m_bIsLateReady = true;
	}

	_int iProcess = m_pParticleCom->Update_Particle(fTimeDelta);
	
	return iProcess;
}

_int CParticle::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;

	
	return 0;
}

void CParticle::Render_GameObject()
{
	if (nullptr == m_pParticleCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	//m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	//m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	
	


	//m_pGraphic_Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_GREATER);
	
	m_pParticleCom->Render_Particle(m_pTextrues[m_eID]);

	//m_pGraphic_Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESS);
	


}

_int CParticle::Ready_State()
{

	switch (m_eID)
	{

	case PARTICLE_SNOW:
		m_Bute.fTimeLife = 1.5f;
		m_Bute.vPosition = CEvent_Manager::GetInstance()->Get_Player_Pos();
		break;
	case PARTICLE_SMOG_A:
		//m_Bute.vPosition = m_vTargetPos;
		//m_Bute.dwColor = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
		m_Bute.fTimeLife = 14.f;
		m_Bute.vPosition = { 225.f,63.f,210.f };
		break;
	case PARTICLE_SMOG_B:
		//m_Bute.vPosition = m_vTargetPos;
		//m_Bute.dwColor = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
		m_Bute.fTimeLife = 14.f;
		m_Bute.vPosition = { 287.f,43.f,245.f };
		break;
	case PARTICLE_SMOG_C:
		//m_Bute.vPosition = m_vTargetPos;
		//m_Bute.dwColor = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
		m_Bute.fTimeLife = 14.f;
		m_Bute.vPosition = { 296.f,71.f,240.f };
		break;
	default:
		break;
	}

	return _int();
}

HRESULT CParticle::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Particle

	switch (m_eID)
	{
	case Client::CParticle::PARTICLE_SNOW:
		m_pParticleCom = (CBuffer_Particle*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_Particle_Snow");
		if (FAILED(Add_Component(L"Com_Particle", m_pParticleCom)))
			return E_FAIL;
		break;
	default:
		break;
	}


	if (m_eID == PARTICLE_SMOG_A || m_eID == PARTICLE_SMOG_B || m_eID == PARTICLE_SMOG_C)
	{
		m_pParticleCom = (CBuffer_Particle*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_Particle_Smoke");
		if (FAILED(Add_Component(L"Com_Particle", m_pParticleCom)))
			return E_FAIL;
	}



	if (FAILED(Ready_Texture()))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CParticle::Ready_Texture()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	switch (m_eID)
	{
	case PARTICLE_SNOW:

		m_pTextrues[PARTICLE_SNOW] = nullptr;
		
		D3DXCreateTextureFromFile(m_pGraphic_Device, 
			L"../../Resources/Particle/0.png", &m_pTextrues[PARTICLE_SNOW]);
		
		if (nullptr == m_pTextrues[PARTICLE_SNOW])
			return E_FAIL;

		break;

	case PARTICLE_END:
		break;
	default:
		break;
	}		

	if (m_eID == PARTICLE_SMOG_A || m_eID == PARTICLE_SMOG_B || m_eID == PARTICLE_SMOG_C)
	{
		m_pTextrues[m_eID] = nullptr;
		D3DXCreateTextureFromFile(m_pGraphic_Device,
			L"../../Resources/Textures/Others/Effect/Smoke_Effect/Smoke_Effect_0.png", &m_pTextrues[m_eID]);

		if (nullptr == m_pTextrues[m_eID])
			return E_FAIL;

	}

	Safe_Release(pManagement);

	return NOERROR;
}

CParticle * CParticle::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CParticle* pInstance = new CParticle(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
		Safe_Release(pInstance);

	return pInstance;
}

Engine::CGameObject * CParticle::Clone_GameEffect(const Engine::_uint &iPARTICLE_ID , const _int iCurNumPlane)
{
	CParticle* pInstance = new CParticle(*this);

	if(FAILED(pInstance->Ready_GameObject((PARTICLE)iPARTICLE_ID,iCurNumPlane)))
		Safe_Release(pInstance);

	return pInstance;
}

void CParticle::Free()
{	
	if (m_bIsColne)
		m_pTextrues[m_eID]->Release();

	Safe_Release(m_pParticleCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pTarget_TransCom);

	CGameObject::Free();
}


CGameObject * CParticle::Clone_GameObject()
{
	return nullptr;
}