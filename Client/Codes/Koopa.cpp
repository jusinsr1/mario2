#include "stdafx.h"
#include "..\Headers\Koopa.h"
#include "Management.h"
#include "Buffer_Player.h"
#include "ObjMove.h"
#include "Collision.h"
#include "Plane.h"
#include "Koopa_Fire.h"

_USING(Client)


CKoopa::CKoopa(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{

}

CKoopa::CKoopa(const CKoopa & rhs)
	:CGameObject(rhs)
{

}

HRESULT CKoopa::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CKoopa::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_MON, this)))
		return -1;

	f = 30.f;
	
	curPlane = 44;
	
	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.017f, TexSize.y*0.017f, TexSize.z);
	
	//m_pTransformCom->SetUp_Speed(20.f, 1);


	m_pTransformCom->SetPlaneNum(4, 3);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(375.f, 150.f, 320.f));
	

	return NOERROR;
}

_int CKoopa::Update_GameObject(const _float & fTImeDelta)
{

	

	if (CEvent_Manager::GetInstance()->Get_LastAxe())
	{			
		f -= 0.8f;

		m_pTransformCom->MoveV3(_vec3(0.f,f*fTImeDelta,0.f));
		return 0;
	}


	AsGravity(fTImeDelta);//중력에의한움직임

	if (curPlane == -1)
		curPlane = prePlane;

	Move_Frame(0.2f, fTImeDelta, 4);

	if (curPlane != CEvent_Manager::GetInstance()->Get_Player_PlaneNum())
		return 0;

	m_fTime += fTImeDelta;	
	
	if (m_fTime > 2.f)
	{
		if (!m_bIsFire)
		{
			CSound_Manager::GetInstance()->PlaySoundW(L"Koopa_Fire.wav", CSound_Manager::EFFECT);
			Fire(fTImeDelta);
			m_bIsFire = true;
		}
		
	}
	if (m_fTime > 3.f)
	{
		m_fTime = 0.f;
		m_bIsFire = false;
		Jump();
	}


	return 0;

}

_int CKoopa::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return _int();
}

void CKoopa::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;
	
	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, TRUE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}




void CKoopa::Coll_GameObject(_int colType)
{
	switch (colType)
	{
	case CCollision::COL_PLAYER:
		break;
	}

}

CKoopa * CKoopa::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CKoopa* pInstance = new CKoopa(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CKoopa Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CKoopa::Clone_GameObject()
{
	CKoopa* pInstance = new CKoopa(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CKoopa Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

_bool CKoopa::GetRC(RC& rc)
{
	rc.plane = curPlane;
	rc.climeDir = climbDir;
	m_pTransformCom->GetRC2D(rc);
	return true;
}

HRESULT CKoopa::Ready_Component()
{

	CManagement* pManagement =  CManagement::GetInstance();
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
	return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
	return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
	return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
	return E_FAIL;

	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
	return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Koopa");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;


	Safe_Release(pManagement);

return NOERROR;
}

void CKoopa::AsGravity(const _float & fTImeDelta)
{
	m_pTransformCom->Move(fTImeDelta, vSpd);

		if (vSpd.y >= -15.f)
		{
			vSpd.y -= 0.5f;
		}	
	
	vSpd.x *= 0.9f;
}

void CKoopa::moveCheck()
{
	if (curPlane % 5 == 4)
		return;


	if (prePlane % 5 != 4)
		return;


	float fCur = 0.f;

	switch ((curPlane % 5) - climbDir)
	{
	case 1:
	case 3:
		fCur = vSpd.y;
		vSpd.y = vSpd.x;
		vSpd.x = -fCur;
		break;
	case -2:
	case 2:
		vSpd.y *= -1.f;
		break;
	case -1:
	case -3:
		fCur = vSpd.y;
		vSpd.y = -vSpd.x;
		vSpd.x = fCur;
		break;
	}

	//= curPlane % 5;

}

void CKoopa::Coll_Move(_vec3& vMove, _int i)
{
	_float vTick = 0.f;

	if (curPlane % 5 == 4)//옥상일때
	{
		switch (climbDir)
		{
		case 0:
			vTick = vMove.z;
			break;
		case 1:
			vTick = vMove.x;
			break;
		case 2:
			vTick = -vMove.z;
			break;
		case 3:
			vTick = -vMove.x;
			break;
		}
	}
	else
	{
		vTick = vMove.y;
	}

	if (i == CCollision::COL_NORMAL || i == CCollision::COL_BOX)
	{
		m_pTransformCom->MoveV3(vMove);
		if (fabs(vTick) < FLT_EPSILON)
		{
			vSpd.x *= -1.f;
		}
	}
	else if (i == CCollision::COL_PLAYER)
	{

			if (fabs(vTick) > FLT_EPSILON)//위에서밟음
			{
				m_iTexIdx = 2;
			}

	}
	else if (i == CCollision::COL_FIRE)
	{

	}
	else if (i == CCollision::COL_MON)
	{
		m_pTransformCom->MoveV3(vMove);
		vSpd.x *= -1.f;
	}

}

void CKoopa::Jump()
{
	if (vSpd.y <= 0.f)
		vSpd.y = 8.f;	
}

void CKoopa::Fire(const _float fTimeDelta)
{
	CManagement* pManagemnet = CManagement::GetInstance();
	pManagemnet->AddRef();

	CKoopa_Fire*	pFire = nullptr;
	if (FAILED(pManagemnet->Add_GameObjectToLayer(L"GameObject_KoopaFire", SCENE_STAGE, L"Layer_Fire", (CGameObject**)&pFire)))
		return;
	

	pFire->SetPlane(curPlane);
	pFire->SetPos(*m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	Safe_Release(pManagemnet);
	
	/*
	m_pSoundManager->PlaySound(L"Fire Ball.wav", CSound_Manager::EFFECT);*/



}

void CKoopa::Free()
{

	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pObjMove);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pPlane);


	CGameObject::Free();
}


