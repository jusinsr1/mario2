#include "stdafx.h"
#include "Coin.h"
#include "Management.h"
#include "Particle.h"
#include "Collision.h"

_USING(Client)

CCoin::CCoin(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CCoin::CCoin(const CCoin & rhs)
	: CGameObject(rhs)
{

}


// Prototype
HRESULT CCoin::Ready_Prototype()
{

	return NOERROR;
}

// Clone
HRESULT CCoin::Ready_GameObject()
{

	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_COIN, this)))
		return -1;

	m_pTransformCom->SetUp_Speed(2.f, 1);

	return NOERROR;
}

_int CCoin::Update_GameObject(const _float & fTimeDelta)
{
	if (m_bIsDead)
	{
		return 1;
	}

	Move_Frame(0.2f, fTimeDelta, 3);

	
	return 0;

}

_int CCoin::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CCoin::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;


	m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();	
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	
}

void CCoin::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation, const _int & iVariable)
{
	m_iNumPlane = iPlaneNum;

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.017f, TexSize.y*0.017f, TexSize.z);



	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);

	m_iTexIdx = iVariable;

	m_pTransformCom->BackWard(0.002f);
}


void CCoin::Coll_GameObject(_int colType)
{
	GET_INSTANCE(CSound_Manager)->PlaySound(L"Coin.wav", CSound_Manager::EFFECT);
	m_bIsDead = true;

}

_bool CCoin::GetRC(RC & rc)
{
	if (rc.plane == m_iNumPlane)
	{
		m_pTransformCom->GetRC2D(rc);
		return true;
	}

	return false;
}

HRESULT CCoin::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	//For.Com_Texture
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Coin");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}

// Create_Prototype
CCoin * CCoin::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCoin*	pInstance = new CCoin(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCoin Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CCoin::Clone_GameObject()
{

	CCoin*	pInstance = new CCoin(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCoin Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCoin::Free()
{

	if (m_bIsColne)
		m_pCollCom->Erase_Object(this);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}