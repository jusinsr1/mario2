#include "stdafx.h"
#include "..\Headers\UI.h"
#include "Management.h"

_USING(Client)

CUI::CUI(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

CUI::CUI(const CUI & rhs)
	:CGameObject(rhs), 
	m_byDrawID(rhs.m_byDrawID)
{
}

HRESULT CUI::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CUI::Ready_GameObject()
{
		if (FAILED(Ready_Component()))
		return E_FAIL;

	m_pTransformCom->SetUp_Speed(5.0f, D3DXToRadian(0.0f));


	return NOERROR;
}

_int CUI::Update_GameObject(const _float & fTimeDelta)
{
	if (!m_bIsInit)
	{
		CManagement*		pManagement = CManagement::GetInstance();
		if (nullptr == pManagement)
			return E_FAIL;

		pManagement->AddRef();

		if (m_byDrawID >= 4)
		{
			m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Num");
			if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
				return E_FAIL;
		}
		else
		{
			m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"UI");
			if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
				return E_FAIL;
		}

		Safe_Release(pManagement);

		if (m_byDrawID == 0)
		{
			// 마리오
			m_pTransformCom->Scaling(150.f, 40.f, 0.f);
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(220.f, 40.f, 0.f));
			m_iTexIdx = 0;
		}
		else if (m_byDrawID == 1)
		{
			// 골드
			m_pTransformCom->Scaling(50.f, 40.f, 0.f);
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(400.f, 50.f, 0.f));
			m_iTexIdx = 1;
		}
		else if (m_byDrawID == 2)
		{
			// 월드
			m_pTransformCom->Scaling(150.f, 60.f, 0.f);
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(650.f, 50.f, 0.f));
			m_iTexIdx = 2;
		}
		else if (m_byDrawID == 3)
		{
			// 타임
			m_pTransformCom->Scaling(150.f, 40.f, 0.f);
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(900.f, 40.f, 0.f));
			m_iTexIdx = 3;
		}
		else if (m_byDrawID >= 4 && m_byDrawID <= 9)
		{

			_float fInteval = 30.f * (m_byDrawID - 4);
			m_pTransformCom->Scaling(20.f, 25.f, 0.f);
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(150.f + fInteval, 80.f, 0.f));
			m_iTexIdx = 0;
		}
		else if (m_byDrawID >= 9)
		{

		_float fInteval = 30.f * (m_byDrawID - 9);
		m_pTransformCom->Scaling(20.f, 25.f, 0.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(420.f + fInteval, 50.f, 0.f));
		m_iTexIdx = 0;
		}


		m_bIsInit = true;
	}

	return _int();
}

_int CUI::LastUpdate_GameObject(const _float & fTimeDelta)
{

	_uint iScore = CEvent_Manager::GetInstance()->Get_Player_Score();
	_uint iNum[6] = {};
	if (iScore >= 999999)
		iScore = 999999;

	iNum[0] = iScore % 10;

	if (iScore >= 100000)
	{
		iNum[5] = iScore / 100000;
		iNum[3] = (iScore - (iNum[5] * 100000)) / 10000;
		iNum[3] = (iScore - (iNum[4] * 10000)) / 1000;
		iNum[2] = (iScore - (iNum[3] * 1000)) / 100;
		iNum[1] = (iScore - (iNum[2] * 100)) / 10;
	}
	else if (iScore >= 10000)
	{
		iNum[4] = iScore / 10000;
		iNum[3] = (iScore - (iNum[4] * 10000)) / 1000;
		iNum[2] = (iScore - (iNum[3] * 1000)) / 100;
		iNum[1] = (iScore - (iNum[2] * 100)) / 10;
	}
	else if (iScore >= 1000)
	{
		iNum[3] = iScore / 1000;
		iNum[2] = (iScore - (iNum[3] * 1000)) / 100;
		iNum[1] = (iScore - (iNum[2] * 100)) / 10;
	}
	else if (iScore >= 100)
	{
		iNum[2] = iScore / 100;
		iNum[1] = (iScore -(iNum[2] * 100)) /10;
	}
	else if (iScore >= 10)
	iNum[1] = iScore / 10;

		if (m_byDrawID == 4)
			m_iTexIdx = iNum[5];
		else if (m_byDrawID == 5)
			m_iTexIdx = iNum[4];
		else if (m_byDrawID == 6)
			m_iTexIdx = iNum[3];
		else if (m_byDrawID == 7)
			m_iTexIdx = iNum[2];
		else if (m_byDrawID == 8)
			m_iTexIdx = iNum[1];
		else if (m_byDrawID == 9)
			m_iTexIdx = iNum[0];


	_uint iCoinCnt = iScore / 6;
	_uint iNumCnt[2] = {};
	if (iCoinCnt >= 99)
		iCoinCnt = 99;

	iNumCnt[0] = iCoinCnt % 10;

		if (iCoinCnt >= 10)
			iNumCnt[1] = iCoinCnt / 10;

		if (m_byDrawID == 10)
			m_iTexIdx = iNumCnt[1];
		else if (m_byDrawID == 11)
			m_iTexIdx = iNumCnt[0];


	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return _int();
}

void CUI::Render_GameObject()
{
	m_pTransformCom->SetUp_OnGraphicDev();
	

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;
	
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, FALSE);
	m_pBufferCom->Render_VIBuffer(&m_pTransformCom->Get_Matrix());
	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, TRUE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

HRESULT CUI::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_ViewPort*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_ViewPort");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}

CUI * CUI::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI* pInstance = new CUI(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("UI Create Failed")
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject * CUI::Clone_GameObject()
{
	CUI*	pInstance = new CUI(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("UI_Clone Create Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}
