#include "stdafx.h"
#include "..\Headers\Sky.h"
#include "Management.h"

_USING(Client)

CSky::CSky(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CSky::CSky(const CSky & rhs)
	: CGameObject(rhs)
{

}


HRESULT CSky::Ready_Prototype()
{
	

	return NOERROR;
}


HRESULT CSky::Ready_GameObject()
{

	
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_pTransformCom->Scaling(1.f, 1.f, 1.f);

	m_pTransformCom->SetUp_Speed(5.0f, D3DXToRadian(90.0f));

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.f, 0.f, 0.f));


	return NOERROR;
}

_int CSky::Update_GameObject(const _float & fTimeDelta)
{
	const _vec3* pPosition = m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	// m_pTransformCom->Rotation_Y(fTimeDelta);
	
	_matrix			matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, (_vec3*)&matView.m[3][0]);

	return _int();
}

_int CSky::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_PRIORITY, this)))
		return -1;


	_matrix			matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, (_vec3*)&matView.m[3][0]);


	return _int();
}

void CSky::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();	

	m_pGraphic_Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(0)))
		return;

	
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, TRUE);


	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	m_pGraphic_Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
}

HRESULT CSky::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (nullptr == m_pTransformCom)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;	

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (nullptr == m_pRendererCom)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Texture
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"SkyBox");
	if (nullptr == m_pTextureCom)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	// For.Com_Buffer
	m_pBufferCom = (CBuffer_CubeTex*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_CubeTex");
	if (nullptr == m_pBufferCom)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CSky * CSky::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CSky*	pInstance = new CSky(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CSky Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


CGameObject * CSky::Clone_GameObject()
{
	CSky*	pInstance = new CSky(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CSky Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CSky::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}
