#include "stdafx.h"
#include "Ending.h"
#include "Management.h"
#include "Collision.h"
#include "Input_Device.h"

_USING(Client)

CEnding::CEnding(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device),
	m_pInput_Device(GET_INSTANCE(CInput_Device))
{
	m_pInput_Device->AddRef();
}

CEnding::CEnding(const CEnding & rhs)
	: CGameObject(rhs), 
	m_pInput_Device(GET_INSTANCE(CInput_Device))
{
	m_pInput_Device->AddRef();
}


// Prototype
HRESULT CEnding::Ready_Prototype()
{
	return NOERROR;
}

// Clone
HRESULT CEnding::Ready_GameObject()
{
		
	if (FAILED(Ready_Component()))
		return E_FAIL;
	
	//if (m_pCollCom == nullptr)
	//	return -1;
	//if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_EVENT, this)))
	//	return -1;

	m_pTransformCom->Scaling(54.f, 30.f, 1.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(380.5f, 150.f, 367.f));
	//m_pTransformCom->SetUp_RotationY(D3DXToRadian(-90.f));

	m_pTransformCom->SetUp_RotationZ(D3DXToRadian(-90.f));
	m_pTransformCom->Rotation_X(D3DXToRadian(90.f));
	m_pTransformCom->Rotation_Y(D3DXToRadian(180.f));

	//fx = 376.f;
	//fy = 153.f;
	//fz = 340.f;

	//fcx = 40.f;
	//	fcy = 40.f;

	return NOERROR;
}

_int CEnding::Update_GameObject(const _float & fTimeDelta)
{	

	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_R))
	//	fcx++;
	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_T))
	//	fcx--;
	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_F))
	//	fcy++;
	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_G))
	//	fcy--;


	//m_pTransformCom->Scaling(fcx, fcy, 1.f);

	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_Q))
	//	fx++;
	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_E))
	//	fx--;

	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_A))
	//	fy++;
	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_D))
	//	fy--;

	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_Z))
	//	fz++;
	//if (CInput_Device::GetInstance()->IsKeyPressing(DIK_C))
	//	fz--;



	//cout <<  "fx :" << fx << endl;
	//cout << "fy :" << fy << endl;
	//cout << "fz :" << fz << endl;

	//cout << "fcx :" << fcx << endl;
	//cout << "fcy :" << fcy << endl;

	//system("cls");


	//m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(fx, fy, fz));

	return _int();
}

_int CEnding::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(
		CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CEnding::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(0)))
		return;
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, TRUE);
}


void CEnding::Input_Key()
{
	//if (m_pInput_Device->IsKeyPressing(DIK_LEFT))
	//else if (m_pInput_Device->IsKeyPressing(DIK_RIGHT))
	//else if (m_pInput_Device->IsKeyPressing(DIK_UP))
}

HRESULT CEnding::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;	

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	 //For.Com_Buffer
 	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
		if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
			return E_FAIL;

	 //For.Com_Texture
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Ending");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;

	//m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	//	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
	//		return E_FAIL;
	


	Safe_Release(pManagement);


	return NOERROR;
}

// Create_Prototype
CEnding * CEnding::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CEnding*	pInstance = new CEnding(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CEnding Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CEnding::Clone_GameObject()
{

	CEnding*	pInstance = new CEnding(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CEnding Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CEnding::Free()
{
	
	if (m_bIsColne)
		//m_pCollCom->Erase_Object(this);
	
	Safe_Release(m_pTextureCom);
	//Safe_Release(m_pCollCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pInput_Device);
	

	CGameObject::Free();
}
