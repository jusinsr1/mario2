#include "stdafx.h"
#include "..\Headers\Koopa.h"
#include "Management.h"
#include "Buffer_Player.h"
#include "ObjMove.h"
#include "Collision.h"
#include "Plane.h"
#include "Koopa_Fire.h"

_USING(Client)


CKoopa_Fire::CKoopa_Fire(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{

}

CKoopa_Fire::CKoopa_Fire(const CKoopa_Fire & rhs)
	: CGameObject(rhs)
{

}

HRESULT CKoopa_Fire::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CKoopa_Fire::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	//curPlane = 44;

	//_vec3 pos = m_pPlane->Find_PlaneInfo(curPlane)->Pos;
	//pos.z += 15.f;

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.017f, TexSize.y*0.017f, TexSize.z);

	m_pTransformCom->SetUp_Speed(20.f, D3DXToRadian(0.f));

	m_pTransformCom->SetPlaneNum(4, 3);


	return NOERROR;
}

_int CKoopa_Fire::Update_GameObject(const _float & fTImeDelta)
{


	if (!m_bIsLateInit)
	{
		m_bIsLateInit = true;
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(m_vTarget.x - 3.f, m_vTarget.y, m_vTarget.z));
	}




	return 0;

}

_int CKoopa_Fire::LastUpdate_GameObject(const _float & fTImeDelta)
{
	m_fTime += fTImeDelta;

	if (m_fTime > 5.f)
		return 1;

	m_pTransformCom->Go_Down(fTImeDelta);

	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return _int();
}

void CKoopa_Fire::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
	
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, TRUE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}




void CKoopa_Fire::Coll_GameObject(_int colType)
{
}

CKoopa_Fire * CKoopa_Fire::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CKoopa_Fire* pInstance = new CKoopa_Fire(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CKoopa_Fire Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CKoopa_Fire::Clone_GameObject()
{
	CKoopa_Fire* pInstance = new CKoopa_Fire(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CKoopa_Fire Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT CKoopa_Fire::Ready_Component()
{

	CManagement* pManagement = CManagement::GetInstance();
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pObjMove = (CObjMove*)pManagement->Clone_Component(SCENE_STAGE, L"Component_ObjMove_Player");
	if (FAILED(Add_Component(L"Com_ObjMove", m_pObjMove)))
		return E_FAIL;

	m_pPlane = (CPlane*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Plane");
	if (FAILED(Add_Component(L"Com_Plane", m_pPlane)))
		return E_FAIL;


	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Koopa_Effect");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;


	Safe_Release(pManagement);

	return NOERROR;
}

void CKoopa_Fire::AsGravity(const _float & fTImeDelta)
{
	m_pTransformCom->Move(fTImeDelta, vSpd);



	if (vSpd.y >= -15.f)
	{
		vSpd.y -= 0.1f;
	}

	vSpd.x *= 0.9f;
}

void CKoopa_Fire::moveCheck()
{
	if (curPlane % 5 == 4)
		return;


	if (prePlane % 5 != 4)
		return;


	float fCur = 0.f;

	switch ((curPlane % 5) - climbDir)
	{
	case 1:
	case 3:
		fCur = vSpd.y;
		vSpd.y = vSpd.x;
		vSpd.x = -fCur;
		break;
	case -2:
	case 2:
		vSpd.y *= -1.f;
		break;
	case -1:
	case -3:
		fCur = vSpd.y;
		vSpd.y = -vSpd.x;
		vSpd.x = fCur;
		break;
	}

	//= curPlane % 5;

}

void CKoopa_Fire::Jump()
{
	if (vSpd.y <= 0.f)
		vSpd.y = 8.f;
}

void CKoopa_Fire::Free()
{

	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pObjMove);
	Safe_Release(m_pPlane);


	CGameObject::Free();
}


