#include "stdafx.h"
#include "..\Headers\LastPang.h"
#include "Management.h"
//#include "Buffer_CubeLine.h"
#include "Collision.h"

_USING(Client)

LastPang::LastPang(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

LastPang::LastPang(const LastPang & rhs)
	: CGameObject(rhs)
{
}



HRESULT LastPang::Ready_Prototype()
{
	return NOERROR;
}

HRESULT LastPang::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	///////////////////////////////////
	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_LAST, this)))
		return -1;

	_vec3 pos = _vec3(376.f,142.f, 332.f);

	m_pTransformCom->Scaling(20.f, 20.f, 20.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &pos);
	
	
	//CManagement* pManagement = GET_INSTANCE(CManagement);
	//if (pManagement == nullptr)
	//	return E_FAIL;
	//pManagement->AddRef();
	//m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Gumba");

	//if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
	//	return E_FAIL;
	//Safe_Release(pManagement);





	return NOERROR;
}

_int LastPang::Update_GameObject(const _float & fTImeDelta)
{
	//이동하는거작성


	m_pTransformCom->MoveV3(_vec3(0.f,0.f,-fTImeDelta*70.f));

	return _int();
}

_int LastPang::LastUpdate_GameObject(const _float & fTImeDelta)
{
	//if (m_pRendererCom == nullptr)
	//	return -1;
	//if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
	//	return -1;

	return _int();
}

void LastPang::Render_GameObject()
{
	//if (m_pBufferCom == nullptr)
	//	return;


	//m_pTransformCom->SetUp_OnGraphicDev();

	//if (FAILED(m_pTextureCom->SetUp_OnGraphicDev()))
	//	return;

	//m_pBufferCom->Render_VIBuffer();


}


void LastPang::GetCUBE(CB & cb)
{
	cb.vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	cb.w = m_pTransformCom->GetCX();
	cb.h = m_pTransformCom->GetCY();
	cb.d = m_pTransformCom->GetCZ();
}






LastPang * LastPang::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	LastPang* pInstance = new LastPang(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CLastPang Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * LastPang::Clone_GameObject()
{
	LastPang* pInstance = new LastPang(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CLastPang Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT LastPang::Ready_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	/*m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_CubeLine*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_CubeLine");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;*/

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;


	Safe_Release(pManagement);

	return NOERROR;
}

void LastPang::Free()
{
	/*Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);*/
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pCollCom);

	CGameObject::Free();
}
