#include "stdafx.h"
#include "..\Headers\Player.h"
#include "Management.h"
#include "Buffer_Player.h"
#include "ObjMove.h"
#include "Collision.h"
#include "Plane.h"
#include "Fire.h"
#include "LastPang.h"

_USING(Client)

const _vec3 CPlayer::Get_Pos()
{
	return *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
}

CPlayer::CPlayer(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
	, m_pInput_Device(GET_INSTANCE(CInput_Device)),
	 m_pManageCom(GET_INSTANCE(CManagement))
	,m_pSoundManager(GET_INSTANCE(CSound_Manager))
{
	m_pInput_Device->AddRef();
	m_pSoundManager->AddRef();
}

CPlayer::CPlayer(const CPlayer & rhs)
	:CGameObject(rhs)
	, m_pInput_Device(rhs.m_pInput_Device),
	m_pManageCom(GET_INSTANCE(CManagement))
	, m_pSoundManager(GET_INSTANCE(CSound_Manager))
{
	m_pInput_Device->AddRef();
	m_pSoundManager->AddRef();
}

HRESULT CPlayer::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CPlayer::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_PLAYER, this)))
		return -1;
	
	curPlane = 35;

	_vec3 pos = m_pPlane->Find_PlaneInfo(curPlane)->Pos;
	pos.z += 15.f;
	
	_vec3 TexSize = *m_pTextureCom[DIR_LEFT]->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.017f, TexSize.y*0.017f, TexSize.z);
	
	m_pTransformCom->SetUp_Speed(20.f, D3DXToRadian(90.f));
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &pos);
	 
	CEvent_Manager*		pEvent_Manager = CEvent_Manager::GetInstance();
	if (nullptr == pEvent_Manager)
		return E_FAIL;
	pEvent_Manager->AddRef();

	if (FAILED(pEvent_Manager->Add_Object_Event_Manager(L"Player", this)))
		return E_FAIL;

	Safe_Release(pEvent_Manager);


	return NOERROR;
}

_int CPlayer::Update_GameObject(const _float & fTImeDelta)
{
	
	BossSound();

	AsGravity(fTImeDelta);//중력에의한움직임
	KeyInput(fTImeDelta);//키인풋
	
	if (curPlane == -1)
		curPlane = prePlane;


	m_pObjMove->Setup_Obj(m_pPlane->Find_PlaneInfo(curPlane),m_pTransformCom,prePlane);//값셋팅

	m_pObjMove->Update();//업데이트
	
	if (m_pObjMove->CheckIn(curPlane,prePlane))//평면이 바뀌진않았는지 체크
	{
		bCheck = true;
		moveCheck();		 //면이바뀜에따른 속도방향전환
	}
	m_pObjMove->Get_ClimbDir(climbDir);//올라온 벽 전해받기
	m_pObjMove->Get_TouchDir(touchDir,offX,offY);//터치방향과 오프셋전해받기

	
	 // Current Time	==========================================
	fireDelay += fTImeDelta;//슈퍼마리오 공격딜레이때문에
	

	switch (m_eCurState)
	{
	case Client::CPlayer::PLAYER_IDLE:
		m_iTexIdx = 4;
		break;
	case Client::CPlayer::PLAYER_WALK:

		Move_Frame(0.1f, fTImeDelta*fSpd, 3);
		break;
	case Client::CPlayer::PLAYER_DOWN:
		m_iTexIdx = 3;
		break;
	case Client::CPlayer::PLAYER_ATTACK:
		break;
	case Client::CPlayer::PLAYER_JUMP:
		
		m_iTexIdx = 5;
		break;
	case Client::CPlayer::PLAYER_END:
		break;
	default:
		break;
	}
	
	_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);


	return _int();
}

_int CPlayer::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;




	return _int();
}

void CPlayer::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;
	
	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom[m_eDir]->SetUp_OnGraphicDev(m_iTexIdx)))
		return;

	if (!bCheck)
		m_pBufferCom->SetUp_BufferPlayer(m_pPlane->Find_PlaneInfo(curPlane), touchDir, climbDir, offX, offY);
	else
		bCheck = false;


	//m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, false);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	//m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, true);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void CPlayer::Coll_GameObject(_int colType)
{
	switch (colType)
	{
	case CCollision::COL_TIRE:
	case CCollision::COL_SPRING:	
		TireJump();
		break;
	case CCollision::COL_COIN:
		m_iScore += 6;
		break;
	case CCollision::COL_AXE:
		if (!m_bIsLast)
		{
			Last();
			m_pSoundManager->PlaySound(L"Blowout.wav", CSound_Manager::EFFECT);
			m_bIsLast = true;
		}
		break;
	}

}

CPlayer * CPlayer::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CPlayer* pInstance = new CPlayer(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CPlayer Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CPlayer::Clone_GameObject()
{
	CPlayer* pInstance = new CPlayer(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CPlayer Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

_bool CPlayer::GetRC(RC& rc)
{
	rc.plane = curPlane;
	rc.climeDir = climbDir;
	m_pTransformCom->GetRC2D(rc);
	return true;
}

HRESULT CPlayer::Ready_Component()
{

	m_pTransformCom = (CTransform*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
	return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
	return E_FAIL;

	m_pRendererCom = (CRenderer*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
	return E_FAIL;

	m_pCollCom = (CCollision*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
	return E_FAIL;

	m_pBufferCom = (CBuffer_Player*)m_pManageCom->Clone_Component(SCENE_STAGE, L"Component_Buffer_Player");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
	return E_FAIL;

	m_pObjMove = (CObjMove*)m_pManageCom->Clone_Component(SCENE_STAGE, L"Component_ObjMove_Player");
	if (FAILED(Add_Component(L"Com_ObjMove", m_pObjMove)))
	return E_FAIL;

	m_pPlane = (CPlane*)m_pManageCom->Clone_Component(SCENE_STAGE, L"Component_Plane");
	if (FAILED(Add_Component(L"Com_Plane", m_pPlane)))
	return E_FAIL;

	if (FAILED(Ready_TextrueCom()))
	return E_FAIL;

return NOERROR;
}

void CPlayer::KeyInput(const _float & fTImeDelta)
{

	if (m_pInput_Device->IsKeyDown(DIK_Z))
	{
		Fire(fTImeDelta);
	}

	if (m_pInput_Device->IsKeyDown(DIK_Y))
	{
		Last();
	}

	if (m_pInput_Device->IsKeyPressing(DIK_Z))
	{
		fSpd = 1.7f;
	}
	
	if (m_pInput_Device->IsKeyUp(DIK_Z))
	{
		fSpd = 1.f;
	}


	if (m_pInput_Device->IsKeyPressing(DIK_LEFT))
	{	
		vSpd.x = -6.f*fSpd;
		m_eDir = DIR_LEFT;

		if (m_eCurState != PLAYER_JUMP && m_eCurState != PLAYER_ATTACK)
			m_eCurState = PLAYER_WALK;
	}
	if (m_pInput_Device->IsKeyUp(DIK_LEFT))
		m_eCurState = PLAYER_IDLE;

	if (m_pInput_Device->IsKeyPressing(DIK_RIGHT))
	{
		vSpd.x = 6.f*fSpd;
		m_eDir = DIR_RIGHT;
		
		if(m_eCurState != PLAYER_JUMP && m_eCurState != PLAYER_ATTACK)
		m_eCurState = PLAYER_WALK;
	}
	if (m_pInput_Device->IsKeyUp(DIK_RIGHT))
		m_eCurState = PLAYER_IDLE;

	if (m_pInput_Device->IsKeyDown(DIK_UP))
	{
		Jump();
		m_eCurState = PLAYER_JUMP;
	}

	if (m_pInput_Device->IsKeyPressing(DIK_DOWN))
		m_eCurState = PLAYER_DOWN;
	if (m_pInput_Device->IsKeyUp(DIK_DOWN))
		m_eCurState = PLAYER_IDLE;

	
		
}

void CPlayer::AsGravity(const _float & fTImeDelta)
{
	m_pTransformCom->Move(fTImeDelta, vSpd);



		if (vSpd.y >= -15.f)
		{
			vSpd.y -= 0.5f;
		}
		
	
	vSpd.x *= 0.9f;
}

void CPlayer::moveCheck()
{
	if (curPlane % 5 == 4)
		return;


	if (prePlane % 5 != 4)
		return;


	float fCur = 0.f;

	switch ((curPlane % 5)- climbDir)
	{
	case 1:
	case 3:
		fCur = vSpd.y;
		vSpd.y = vSpd.x;
		vSpd.x = -fCur;
		break;
	case -2:
	case 2:
		vSpd.y *= -1.f;
		break;
	case -1:
	case -3:
		fCur = vSpd.y;
		vSpd.y = -vSpd.x;
		vSpd.x = fCur;
		break;
	}

	 //= curPlane % 5;

}

void CPlayer::BossSound()
{
	if (curPlane >= 40 && !m_bIsBgm)
	{
		m_pSoundManager->StopSound(CSound_Manager::BGM);
		m_pSoundManager->PlayBGM(L"Boss.mp3");
		m_bIsBgm = true;
	}


}

void CPlayer::Fire(const _float & fTImeDelta)
{
	if (fireDelay > 0.5f)
	{
		CFire*	pFire = nullptr;
		if (FAILED(m_pManageCom->Add_GameObjectToLayer(L"GameObject_Fire", SCENE_STAGE, L"Layer_Fire", (CGameObject**)&pFire)))
			cout << "불실패" << endl;
		m_pSoundManager->PlaySound(L"Fire Ball.wav", CSound_Manager::EFFECT);

		//cout << m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT)->z << endl;

		pFire->setData(curPlane, m_pTransformCom, climbDir, m_eDir);
		
	



		fireDelay = 0.f;
	}
}



void CPlayer::Last()
{
	
	LastPang*	pLast = nullptr;
	if (FAILED(m_pManageCom->Add_GameObjectToLayer(L"GameObject_LastPang", SCENE_STAGE, L"Layer_Last", (CGameObject**)&pLast)))
		cout << "라스트팡실패" << endl;
		
}




void CPlayer::Coll_Move(_vec3& vMove, _int i)
{

	_float vTick = 0.f;

	if (curPlane % 5 == 4)//옥상일때
	{
		switch (climbDir)
		{
		case 0:
			vTick = vMove.z;
			break;
		case 1:
			vTick = vMove.x;
			break;
		case 2:
			vTick = -vMove.z;
			break;
		case 3:
			vTick = -vMove.x;
			break;
		}
	}
	else
	{
		vTick = vMove.y;
	}

	


	if (i == CCollision::COL_FLAT)//플렛폼일때만 아래로 내려갈때만 
	{


		if (fabs(vTick) > 0.f)
		{
			if (vSpd.y < 0.f)
			{
				m_pTransformCom->MoveV3(vMove);//속도가 아래를향할때만 위치보정
				vSpd.y = 0.f;
				cntJump = 0;
				m_eCurState = PLAYER_IDLE;
			}
		}

	}
	else if (i == CCollision::COL_BOX)
	{
		if (vSpd.y < 0.f)
		{
			vSpd.y = -3.f;
			cntJump = 0;
			m_eCurState = PLAYER_IDLE;
		}
			
		m_pTransformCom->MoveV3(vMove);
	}
	else if (i == CCollision::COL_MON)
	{

		if (vTick > FLT_EPSILON)//위에서 부딛힘
		{
			m_pTransformCom->MoveV3(vMove);
			vSpd.y = 8.f;
			m_eCurState = PLAYER_JUMP;
		}
		
		if (m_eCurState != PLAYER_JUMP || m_eCurState != PLAYER_ATTACK)
			m_iCnt++;


	}
	else
	{
		m_pTransformCom->MoveV3(vMove);

		if (vTick > 0.f&&vSpd.y <= 0.f)
		{
			vSpd.y = 0.f;
			cntJump = 0;
			m_eCurState = PLAYER_IDLE;

		}
		else if ( vTick < 0.f&&vSpd.y>0.f)
		{
			vSpd.y = -0.1f;
			m_eCurState = PLAYER_JUMP;
		}

	}

}

void CPlayer::Jump()
{
	if (cntJump == 0)
	{
		m_pTransformCom->MoveV3({0.f,0.03f,0.f});
		vSpd.y = 10.f;
		//vSpd.y += 20.f;
		m_pSoundManager->PlaySound(L"Jump.wav", CSound_Manager::EFFECT);
		cntJump++;	
	}
	else if (cntJump == 1)
	{
		vSpd.y += 5.f;
		m_pSoundManager->PlaySound(L"Jump.wav", CSound_Manager::EFFECT);
		cntJump++;	
	}

}

void CPlayer::TireJump()
{

	if (vSpd.y <= 0.f)
		vSpd.y = 18.f;
		
}

void CPlayer::Free()
{

	//if (m_bIsColne)
	//	m_pCollCom->Release_Coll(this);


	for (size_t i = 0; i < DIR_END; i++)
		Safe_Release(m_pTextureCom[i]);
	Safe_Release(m_pSoundManager);
	//Safe_Release(m_pManageCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pInput_Device);
	Safe_Release(m_pObjMove);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pPlane);


	CGameObject::Free();
}


//==============================================================================================================
// 텍스쳐 클론
//==============================================================================================================

HRESULT CPlayer::Ready_TextrueCom()
{

	m_pTextureCom[DIR_LEFT] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Mario_L");
	if (FAILED(Add_Component(L"Com_Texture_Idle_Left", m_pTextureCom[DIR_LEFT])))
		return E_FAIL;
	m_pTextureCom[DIR_RIGHT] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Mario_R");
	if (FAILED(Add_Component(L"Com_Texture_Idle_Right", m_pTextureCom[DIR_RIGHT])))
		return E_FAIL;

	return NOERROR;
}
