#include "stdafx.h"
#include "Platform.h"
#include "Management.h"
#include "Collision.h"


_USING(Client)

CPlatform::CPlatform(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CPlatform::CPlatform(const CPlatform & rhs)
	: CGameObject(rhs)
{

}


// Prototype
HRESULT CPlatform::Ready_Prototype()
{

	return NOERROR;
}

// Clone
HRESULT CPlatform::Ready_GameObject()
{

	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_FLAT, this)))
		return -1;


	return NOERROR;
}

_int CPlatform::Update_GameObject(const _float & fTimeDelta)
{


	return _int();
}

_int CPlatform::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return 0;
}

void CPlatform::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CPlatform::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation)
{
	m_iNumPlane = iPlaneNum;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);

	

	
	random_device random;
	mt19937 engine(random());
	uniform_int_distribution<int> distribution(0, m_pTextureCom->GetTexSize()-1);

	m_iTexIdx = distribution(engine);

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.017f, TexSize.y*0.017f, TexSize.z);

	climeDir = iRotation;
}

void CPlatform::Coll_GameObject(_int colType)
{
}

_bool CPlatform::GetRC(RC & rc)
{
	if (rc.climeDir != climeDir&&rc.plane%5==4)
		return false;


	if (rc.plane == m_iNumPlane)
	{
		m_pTransformCom->GetRC2D(rc);
		return true;
	}
	else
		return false;
		
//	}
	
	
}

HRESULT CPlatform::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	//For.Com_Texture
	m_pTextureCom= (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"On_Platform");
	if (FAILED(Add_Component(L"Com_TextureA", m_pTextureCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
		return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}

// Create_Prototype
CPlatform * CPlatform::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CPlatform*	pInstance = new CPlatform(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CPlatform Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CPlatform::Clone_GameObject()
{

	CPlatform*	pInstance = new CPlatform(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CPlatform Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CPlatform::Free()
{

	Safe_Release(m_pTextureCom);

	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pCollCom);

	CGameObject::Free();
}