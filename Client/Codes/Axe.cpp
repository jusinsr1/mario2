#include "stdafx.h"
#include "Axe.h"
#include "Management.h"
#include "Collision.h"
#include "Input_Device.h"

_USING(Client)

CAxe::CAxe(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device),
	m_pInput_Device(GET_INSTANCE(CInput_Device))
{
	m_pInput_Device->AddRef();
}

CAxe::CAxe(const CAxe & rhs)
	: CGameObject(rhs),
	m_pInput_Device(GET_INSTANCE(CInput_Device))
{
	m_pInput_Device->AddRef();
}


void CAxe::Coll_GameObject(_int colType)
{
	m_bIsLast = true;
}

_bool CAxe::GetRC(RC & rc)
{

	rc.plane = curPlane;
	rc.climeDir = climbDir;
	m_pTransformCom->GetRC2D(rc);

	return true;

}

// Prototype
HRESULT CAxe::Ready_Prototype()
{
	return NOERROR;
}

// Clone
HRESULT CAxe::Ready_GameObject()
{

	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_AXE, this)))
		return -1;

	CEvent_Manager::GetInstance()->Add_Object_Event_Manager(L"Axe", this);

	m_pTransformCom->Scaling(1.f, 2.f, 1.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(380.f, 151.f, 347.f));
	

	m_pTransformCom->SetUp_RotationZ(D3DXToRadian(-90.f));
	m_pTransformCom->Rotation_X(D3DXToRadian(90.f));
	m_pTransformCom->Rotation_Y(D3DXToRadian(180.f));

	return NOERROR;
}

_int CAxe::Update_GameObject(const _float & fTimeDelta)
{
	if (m_bIsLast)
	{

		m_fTime += fTimeDelta;

		Move_Frame(0.2f, fTimeDelta, 3);

		if (m_fTime > 4.f)
			return 1;
	}



	return _int();
}

_int CAxe::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(
		CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CAxe::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(0)))
		return;
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, TRUE);
}


HRESULT CAxe::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	//For.Com_Texture
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Axe");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
		if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
			return E_FAIL;



	Safe_Release(pManagement);


	return NOERROR;
}

// Create_Prototype
CAxe * CAxe::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CAxe*	pInstance = new CAxe(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CAxe Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CAxe::Clone_GameObject()
{

	CAxe*	pInstance = new CAxe(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CAxe Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CAxe::Free()
{

	if (m_bIsColne)
	{
		m_pCollCom->Erase_Object(this);
		CEvent_Manager::GetInstance()->Erase_Event_Manager(L"Axe");
	}
	

	Safe_Release(m_pTextureCom);
	Safe_Release(m_pCollCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pInput_Device);


	CGameObject::Free();
}
