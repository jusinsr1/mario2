#include "stdafx.h"
#include "..\Headers\Sound_Manager.h"

_USING(Client)
_IMPLEMENT_SINGLETON(CSound_Manager)

CSound_Manager::CSound_Manager()
{
}

void CSound_Manager::Ready_Sound()
{
	FMOD_System_Create(&m_pSystem);
	FMOD_System_Init(m_pSystem, END, FMOD_INIT_NORMAL, nullptr);

	LoadSoundFile();
}

void CSound_Manager::UpdateSound()
{
	FMOD_System_Update(m_pSystem);
}

void CSound_Manager::PlaySound(const TCHAR * pSoundKey, CHANNEL_ID eID)
{
	auto& iter_find = find_if(m_MapSound.begin(), m_MapSound.end(),
		[&pSoundKey](auto& MyPair)
	{
		return !lstrcmp(pSoundKey, MyPair.first);
	});

	if (m_MapSound.end() == iter_find)
		return;

	FMOD_System_PlaySound(m_pSystem, FMOD_CHANNEL_FREE, iter_find->second, FALSE,
		&m_pChannel[eID]);

	FMOD_System_Update(m_pSystem);
}

void CSound_Manager::PlayBGM(const TCHAR * pSoundKey)
{
	auto& iter_find = find_if(m_MapSound.begin(), m_MapSound.end(),
		[&pSoundKey](auto& MyPair)
	{
		return !lstrcmp(pSoundKey, MyPair.first);
	});

	if (m_MapSound.end() == iter_find)
		return;

	FMOD_System_PlaySound(m_pSystem, FMOD_CHANNEL_FREE, iter_find->second, FALSE,
		&m_pChannel[BGM]);

	FMOD_Channel_SetMode(m_pChannel[BGM], FMOD_LOOP_NORMAL);
	FMOD_System_Update(m_pSystem);
}

void CSound_Manager::StopSound(CHANNEL_ID eID)
{
	FMOD_Channel_Stop(m_pChannel[eID]);
}

void CSound_Manager::StopAll()
{
	for (int i = 0; i < END; ++i)
		FMOD_Channel_Stop(m_pChannel[i]);
}

void CSound_Manager::LoadSoundFile()
{
	_finddata_t fd;
	int handle = _findfirst("../../Resources/Sound/*.*", &fd);

	if (-1 == handle)
		return;

	char szRelativePath[128] = "../../Resources/Sound/";
	char szFullPath[128] = "";

	int iResult = 0;

	while (-1 != iResult)
	{
		strcpy_s(szFullPath, szRelativePath);
		strcat_s(szFullPath, fd.name);

		FMOD_SOUND* pSound = nullptr;
		FMOD_RESULT eRes = FMOD_System_CreateSound(m_pSystem, szFullPath, FMOD_HARDWARE,
			nullptr, &pSound);

		if (FMOD_OK == eRes)
		{
			int iCount = strlen(fd.name) + 1;
			TCHAR* pSoundKey = new TCHAR[iCount];
			ZeroMemory(pSoundKey, sizeof(TCHAR) * iCount);

			MultiByteToWideChar(CP_ACP, 0, fd.name, iCount, pSoundKey, iCount);

			if (false == m_MapSound.insert({ pSoundKey, pSound }).second)
			{
				delete[] pSoundKey;
				FMOD_Sound_Release(pSound);
			}
		}

		iResult = _findnext(handle, &fd);
	}

	FMOD_System_Update(m_pSystem);
}

void CSound_Manager::Free()
{
	for (auto& MyPair : m_MapSound)
	{
		delete[] MyPair.first;
		FMOD_Sound_Release(MyPair.second);
	}

	m_MapSound.clear();

	FMOD_System_Release(m_pSystem);
	FMOD_System_Close(m_pSystem);
}
