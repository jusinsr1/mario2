#include "stdafx.h"
#include "..\Headers\Fire.h"

#include "Management.h"
#include "Buffer_Player.h"
#include "Collision.h"
#include "Plane.h"
#include "ObjMove.h"

_USING(Client)
CFire::CFire(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device), m_pManageCom(GET_INSTANCE(CManagement))
{
}


CFire::CFire(const CFire & rhs)
	: CGameObject(rhs), m_pManageCom(GET_INSTANCE(CManagement))
{
}

HRESULT CFire::Ready_Prototype()
{
	return NOERROR;
}


HRESULT CFire::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_FIRE, this)))
		return -1;

	curPlane = 1;
	_vec3 pos = m_pPlane->Find_PlaneInfo(curPlane)->Pos;

	

	return NOERROR;
}



_int CFire::Update_GameObject(const _float & fTImeDelta)
{
	if (bDead)
		return 1;



	AsGravity(fTImeDelta);//중력에의한움직임

	if (curPlane == -1)
		curPlane = prePlane;



	m_pObjMove->Setup_Obj(m_pPlane->Find_PlaneInfo(curPlane), m_pTransformCom,prePlane);//값셋팅


	m_pObjMove->Update();//업데이트



	if (m_pObjMove->CheckIn(curPlane, prePlane))//평면이 바뀌진않았는지 체크
	{
		bCheck = true;
		moveCheck();		 //면이바뀜에따른 속도방향전환
	}

	m_pObjMove->Get_ClimbDir(climbDir);//올라온 벽 전해받기

	m_pObjMove->Get_TouchDir(touchDir, offX, offY);//터치방향과 오프셋전해받기

	Move_Frame(0.01f, fTImeDelta, m_pTextureCom->GetTexSize());



	return _int();
}



_int CFire::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	//m_iNumPlane = curPlane;


	return _int();
}



void CFire::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;

	if (!bCheck)
		m_pBufferCom->SetUp_BufferPlayer(m_pPlane->Find_PlaneInfo(curPlane), touchDir, climbDir, offX, offY);
	else
		bCheck = false;


	
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}



CFire * CFire::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CFire* pInstance = new CFire(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CFire Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CFire::Clone_GameObject()
{
	CFire* pInstance = new CFire(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CFire Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

_bool CFire::GetRC(RC& rc)
{
	rc.plane = curPlane;
	rc.climeDir = climbDir;
	m_pTransformCom->GetRC2D(rc);
	return true;
}




HRESULT CFire::Ready_Component()
{

	m_pTransformCom = (CTransform*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_Player*)m_pManageCom->Clone_Component(SCENE_STAGE, L"Component_Buffer_Player");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pObjMove = (CObjMove*)m_pManageCom->Clone_Component(SCENE_STAGE, L"Component_ObjMove_Player");
	if (FAILED(Add_Component(L"Com_ObjMove", m_pObjMove)))
		return E_FAIL;

	m_pPlane = (CPlane*)m_pManageCom->Clone_Component(SCENE_STAGE, L"Component_Plane");
	if (FAILED(Add_Component(L"Com_Plane", m_pPlane)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Skill_Effect");
	if (FAILED(Add_Component(L"Com_Texture", m_pPlane)))
		return E_FAIL;

	return NOERROR;
}


void CFire::AsGravity(const _float & fTImeDelta)
{
	m_pTransformCom->Move(fTImeDelta, vSpd);


	if (vSpd.y >= -30.f)
	{
		vSpd.y -= 4.f;
	}

}


void CFire::moveCheck()
{
	if (curPlane % 5 == 4)
		return;


	if (prePlane % 5 != 4)
		return;


	float fCur = 0.f;

	switch ((curPlane % 5) - climbDir)
	{
	case 1:
	case 3:
		fCur = vSpd.y;
		vSpd.y = vSpd.x;
		vSpd.x = -fCur;
		break;
	case -2:
	case 2:
		vSpd.y *= -1.f;
		break;
	case -1:
	case -3:
		fCur = vSpd.y;
		vSpd.y = -vSpd.x;
		vSpd.x = fCur;
		break;
	}

	//= curPlane % 5;

}



void CFire::Coll_Move(_vec3& vMove, _int i)
{


	if (i == CCollision::COL_MON)
		bDead = true;

	_float vTick = 0.f;

	if (curPlane % 5 == 4)//옥상일때
	{
		switch (climbDir)
		{
		case 0:
			vTick = vMove.z;
			break;
		case 1:
			vTick = vMove.x;
			break;
		case 2:
			vTick = -vMove.z;
			break;
		case 3:
			vTick = -vMove.x;
			break;
		}
	}
	else
	{
		vTick = vMove.y;
	}

	
	if (vTick < FLT_EPSILON)
	{
		bDead = true;
	}
	else
	{
		m_pTransformCom->MoveV3(vMove);
		vSpd.y *= -1.f;
	}




}

void CFire::Free()
{
	if (m_bIsColne)
	{
		m_pCollCom->Erase_Object(this);
	}


	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pObjMove);
	//Safe_Release(m_pGraphic_Device);
	Safe_Release(m_pCollCom);
	Safe_Release(m_pPlane);


	CGameObject::Free();
}

void CFire::setData(_int _curplane, CTransform* trs, _int climeDir, _int right)
{
	curPlane = _curplane;
	prePlane = _curplane;

	_vec3 vPos = *trs->Get_StateInfo(CTransform::STATE_POSITION);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION,&vPos);

	m_pTransformCom->SetRUL(trs);


	
	
	

	if (right == 0)
		vSpd.x = -30.f;
	else
		vSpd.x = 30.f;

	vSpd.y = 5.f;

	climbDir = Direction(climeDir);

}
