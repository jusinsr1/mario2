#include "stdafx.h"
#include "Smoke.h"
#include "Management.h"
#include "Particle.h"


_USING(Client)

CSmoke::CSmoke(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CSmoke::CSmoke(const CSmoke & rhs)
	: CGameObject(rhs)
{

}


// Prototype
HRESULT CSmoke::Ready_Prototype()
{

	return NOERROR;
}

// Clone
HRESULT CSmoke::Ready_GameObject()
{

	if (FAILED(Ready_Component()))
		return E_FAIL;

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(30.f, 10.f, 20.f));

	m_pTransformCom->SetUp_Speed(2.f, 1);

	return NOERROR;
}

_int CSmoke::Update_GameObject(const _float & fTimeDelta)
{

	if (FAILED(Late_Ready_GameObject()))
		return -1;

	_int iProcess = Update_State(fTimeDelta);

	if (-1 == iProcess)
		return -1;


	return iProcess;

}

_int CSmoke::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	if (0x8000 & GetAsyncKeyState('Z'))
		m_eCurState = STATE_IDLE;

	return 0;
}

void CSmoke::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;


	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();	
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	
}

void CSmoke::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation, const _int & iVariable)
{
	m_iNumPlane = iPlaneNum;

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x * 0.02f, TexSize.y * 0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);

	m_iTexIdx = iVariable;
}

HRESULT CSmoke::Late_Ready_GameObject()
{
	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case Client::CSmoke::STATE_NONE:
			break;
		case Client::CSmoke::STATE_IDLE:	
			break;
		case Client::CSmoke::STATE_END:
			break;
		default:
			break;
		}
		m_ePreState = m_eCurState;
	}

	return NOERROR;
}

_int CSmoke::Update_State(const _float & fTimeDelta)
{
	switch (m_eCurState)
	{
	case Client::CSmoke::STATE_NONE:
		break;
	case Client::CSmoke::STATE_IDLE:
		
		m_fSeta-= 1.5f;

		if (m_fSeta < -80.f && !m_bIsParticle)
		{
			CParticle* pEffect = nullptr;
			CManagement* pManagement = CManagement::GetInstance();
			pManagement->AddRef();
			pManagement->Add_GameEffectToLayer(L"Particle", SCENE_STAGE, L"Effect_Layer", _uint(CParticle::PARTICLE_ITEM),
				m_iNumPlane, (CGameObject**)&pEffect);
			dynamic_cast<CParticle*>(pEffect)->Set_TargetTransCom(m_pTransformCom);
			m_pTransformCom->AddRef();
			dynamic_cast<CParticle*>(pEffect)->SetNumPlane(m_iTypePlane);
			m_bIsParticle = true;

			Safe_Release(pManagement);

		}
		if (m_fSeta < -90.f)
			m_eCurState = STATE_END;
		else
		{
			m_pTransformCom->BackWard(fTimeDelta);
			m_pTransformCom->Rotation_AxisRight(fTimeDelta, m_fSeta);
		}

		break;
	case Client::CSmoke::STATE_END:
		return 1;
		break;
	default:
		break;
	}

	return _int();
}

HRESULT CSmoke::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	//For.Com_Texture
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Item");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}

// Create_Prototype
CSmoke * CSmoke::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CSmoke*	pInstance = new CSmoke(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CSmoke Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CSmoke::Clone_GameObject()
{

	CSmoke*	pInstance = new CSmoke(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CSmoke Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CSmoke::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}