#include "stdafx.h"
#include "..\Headers\LastBuilding.h"
#include "Management.h"
#include "Buffer_CubeLine.h"
#include "Collision.h"

_USING(Client)

LastBuilding::LastBuilding(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

LastBuilding::LastBuilding(const LastBuilding & rhs)
	: CGameObject(rhs)
{
}

HRESULT LastBuilding::Ready_Prototype()
{
	return NOERROR;
}

HRESULT LastBuilding::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	////////////////////////////////
	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_LASTBUILDING, this)))
		return -1;

	vSpd = {0.f,0.f,0.f};

	return NOERROR;
}

_int LastBuilding::Update_GameObject(const _float & fTImeDelta)
{
	if (bLast)
	{
		vSpd.y -= 2.f*fTImeDelta;
	}

	
	m_pTransformCom->MoveV3(vSpd);
	return _int();
}

_int LastBuilding::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void LastBuilding::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;


	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev()))
		return;

	m_pBufferCom->Render_VIBuffer();


}

void LastBuilding::GetCUBE(CB & cb)
{
	cb.vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	cb.w = m_pTransformCom->GetCX();
	cb.h = m_pTransformCom->GetCY();
	cb.d = m_pTransformCom->GetCZ();
}

void LastBuilding::Coll_GameObject(_int colType)
{

	if (colType == CCollision::COL_LAST)
	{
		bLast = true;
	}


}

HRESULT LastBuilding::SetData(_vec3 vecScale, _vec3 vecPos, const _tchar* ImgTag)
{
	m_pTransformCom->Scaling(vecScale.x, vecScale.y, vecScale.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vecPos);
	CManagement* pManagement = GET_INSTANCE(CManagement);


	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, ImgTag);

	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;
	Safe_Release(pManagement);

	return NOERROR;
}

LastBuilding * LastBuilding::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	LastBuilding* pInstance = new LastBuilding(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CBuilding Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * LastBuilding::Clone_GameObject()
{
	LastBuilding* pInstance = new LastBuilding(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CBuilding Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT LastBuilding::Ready_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_CubeLine*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_CubeLine");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

void LastBuilding::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pCollCom);

	CGameObject::Free();
}
