#include "stdafx.h"
#include "..\Headers\Heart.h"
#include "Management.h"

_USING(Client)

CHeart::CHeart(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

CHeart::CHeart(const CHeart & rhs)
	:CGameObject(rhs), 
	m_byDrawID(rhs.m_byDrawID)
{
}

HRESULT CHeart::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CHeart::Ready_GameObject()
{
		if (FAILED(Ready_Component()))
		return E_FAIL;

	m_pTransformCom->SetUp_Speed(5.0f, D3DXToRadian(0.0f));


	return NOERROR;
}

_int CHeart::Update_GameObject(const _float & fTimeDelta)
{
	if (!m_bIsInit)
	{
		CManagement*		pManagement = CManagement::GetInstance();
		if (nullptr == pManagement)
			return E_FAIL;

		pManagement->AddRef();

		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Heart");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;

		Safe_Release(pManagement);

		_float fInterval = m_byDrawID * 50.f;

		m_pTransformCom->Scaling(40.f, 40.f, 0.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(1100.f + fInterval, 40.f, 0.f));
		m_iTexIdx = 0;

		m_bIsInit = true;
	}


	return _int();
}

_int CHeart::LastUpdate_GameObject(const _float & fTimeDelta)
{

	_uint iHitCnt = CEvent_Manager::GetInstance()->Get_Player_HitCnt();

	if (iHitCnt == 1)
	{
		if (m_byDrawID == 2)
			Down_Heart(fTimeDelta);
	}
	else if (iHitCnt == 2)
	{
		if (m_byDrawID == 1)
			Down_Heart(fTimeDelta);
	}
	else if (iHitCnt == 3)
	{
		if (m_byDrawID == 0)
			Down_Heart(fTimeDelta);
	}


	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return _int();
}

void CHeart::Render_GameObject()
{
	m_pTransformCom->SetUp_OnGraphicDev();
	

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;
	
	
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, FALSE);
	m_pBufferCom->Render_VIBuffer(&m_pTransformCom->Get_Matrix());
	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, TRUE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
}

HRESULT CHeart::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_ViewPort*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_ViewPort");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}

void CHeart::Down_Heart(const _float fTimeDelta)
{
	if (m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y < 0.f)
		return;

	random_device random;
	mt19937 engine(random());
	uniform_int_distribution<int> distribution(40, 50);

	_int iRand = distribution(engine);

	if (fTimeDelta > 1.f)
		return;

	m_fTime += fTimeDelta;

	if (m_fTime > 1.f)
	{
		m_pTransformCom->Go_Up(fTimeDelta + 5.f);
		m_iTexIdx = 1;
	}
	else if (m_fTime < 10.f)
	{
		if (iRand % 2 == 0)
			m_pTransformCom->Go_Left(fTimeDelta + iRand * 0.01f);
		else
			m_pTransformCom->Go_Right(fTimeDelta + iRand * 0.01f);
	}

}

CHeart * CHeart::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CHeart* pInstance = new CHeart(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("UI Create Failed")
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject * CHeart::Clone_GameObject()
{
	CHeart*	pInstance = new CHeart(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("UI_Clone Create Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CHeart::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}
