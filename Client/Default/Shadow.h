#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;

_END

_BEGIN(Client)
class CBuffer_Shadow;
class CShadow final : public CGameObject
{
private:
	explicit CShadow(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CShadow(const CShadow& rhs);
	virtual ~CShadow() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();

	HRESULT SetData(_vec3 vecScale, _vec3 vecPos, const _tchar* ImgTag);

public:
	static CShadow* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
private:
	HRESULT Ready_Component();

private:
	virtual void Free();

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_Shadow*	    m_pBufferCom = nullptr;
};

_END