#pragma once
#include "Base.h"

_BEGIN(Engine)
_END
_BEGIN(Client)

class CSound_Manager final:	public CBase
{
	_DECLARE_SINGLETON(CSound_Manager)
public:
	enum CHANNEL_ID { BGM, PLAYER, MONSTER, EFFECT, END };

private:
	explicit CSound_Manager();
	virtual ~CSound_Manager() = default;

public:
	void Ready_Sound();
	void UpdateSound();
	void PlaySound(const TCHAR* pSoundKey, CHANNEL_ID eID);
	void PlayBGM(const TCHAR* pSoundKey);
	void StopSound(CHANNEL_ID eID);
	void StopAll();

private:
	void LoadSoundFile();

private:
	FMOD_SYSTEM*	m_pSystem;
	FMOD_CHANNEL*	m_pChannel[END];
	map<const TCHAR*, FMOD_SOUND*>	m_MapSound;

	virtual void Free() override;
};

_END