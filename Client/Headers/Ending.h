#pragma once

#include "GameObject.h"
#include "Input_Device.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
_END

_BEGIN(Client)

class CEnding final : 
	public CGameObject
{
private:
	explicit CEnding(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CEnding(const CEnding& rhs);
	virtual ~CEnding() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
//public:
//	virtual void		Coll_GameObject(_int colType);
//	virtual _bool		GetRC(RC& rc);
private:
	CInput_Device*		m_pInput_Device = nullptr;
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	//CCollision*			m_pCollCom = nullptr;
private:
	_bool				m_bIsReady = false;

private:
	void				Input_Key();

	_float				fx = 0.f, fy = 0.f, fz = 0.f;
	_float				fcx = 0.f, fcy = 0.f, fcz = 0.f;
private:
	HRESULT				Ready_Component();

public:
	static CEnding*			Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

};

_END