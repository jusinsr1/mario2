#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CCollision;
_END

_BEGIN(Client)

class CJumping final :
	public CGameObject
{
public: enum JUMPING_STATE { STATE_NONE, STATE_JUMP , STATE_END };

private:
	explicit CJumping(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CJumping(const CJumping& rhs);
	virtual ~CJumping() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation);
public:
	virtual void		Coll_GameObject(_int colType);
	virtual _bool		GetRC(RC& rc);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CCollision*			m_pCollCom = nullptr;
private:
	JUMPING_STATE		m_eCurState = STATE_NONE;
	JUMPING_STATE		m_ePreState = STATE_END;
private:
	_int					m_iNumPlane = -1;
private:
	_bool					m_bTime = false;
private:
	HRESULT		Late_Ready_GameObject();
	_int		Update_State(const _float& fTimeDelta);

private:
	HRESULT Ready_Component();
public:
	static CJumping*			Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

};

_END