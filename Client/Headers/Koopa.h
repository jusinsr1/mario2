#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CCollision;
class CBuffer_RcTex;
_END

_BEGIN(Client)
class CObjMove;
class CPlane;
class CKoopa final : public CGameObject
{
public:
	enum Koopa	{ Koopa_IDLE, Koopa_ATTACK, Koopa_DEAD, Koopa_END };

private:
	explicit CKoopa(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CKoopa(const CKoopa& rhs);
	virtual ~CKoopa() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();


	virtual void Coll_GameObject(_int colType);
	CObjMove* GetObjMove(){ return m_pObjMove; }
public:
	static CKoopa* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();

public:
	virtual _bool GetRC(RC& rc);
private:
	HRESULT Ready_Component();
private:
	void AsGravity(const _float & fTImeDelta);
	void moveCheck();
public:
	virtual void Coll_Move(_vec3& vMove, _int i);
private:
	void Jump();
	void Fire(const _float fTimeDelta);
private:
	virtual void Free();
private:
	Koopa			m_eCurState = Koopa_IDLE;	
	Koopa			m_ePreState = Koopa_END;

	_float			m_fTimeLast = 0.f;

	_float			f = 0.f;

private:
	CRenderer*		m_pRendererCom = nullptr;
	CCollision*		m_pCollCom = nullptr;
	CTransform*		m_pTransformCom = nullptr;
	CObjMove*		m_pObjMove = nullptr;
	CBuffer_RcTex*	m_pBufferCom = nullptr;
	CManagement*	m_pManageCom = nullptr;
	CPlane*			m_pPlane = nullptr;
private:
	CTexture*		m_pTextureCom = nullptr;
private:
	_int curPlane = 0;
	_int prePlane = 0;

	Direction climbDir = Direction(0);//옥상으로 올라올때  올라온 면 판정때문에 필요
	Direction touchDir = Direction(0);

	_float offX = 0.f;
	_float offY = 0.f;

	_bool bCheck = false;
	_bool m_bIsFire = false;

	_vec2 vSpd = {0.f,0.f};
	_float fSpd = 1.f;

	_uint cntJump = 0;


	_float fireDelay = 0.f;



};

_END