#pragma once
#include "Effect.h"
#include "Management.h"

_BEGIN(Engine)
class CCollision;
_END

_BEGIN(Client)

class CEffect_Player :
	public CEffect
{

public:
	enum EFF_PLAYER { EFF_BULLET, EFF_END };

public:
	void	Set_TargetTransCom(CTransform* pTarget_TransCom) { m_pTarget_TransCom = pTarget_TransCom; }
	void	SetNumPlane(_int& iNumPlane) { m_iNumPlane = iNumPlane; }
public:
	virtual void		Coll_GameObject(_int colType);
	virtual _bool		GetRC(RC& rc);
public:
	explicit CEffect_Player(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CEffect_Player(CEffect_Player& rhs);
	virtual	 ~CEffect_Player() = default;
public:
	virtual HRESULT		Ready_Prototype();
	virtual HRESULT		Ready_GameObject(EFF_PLAYER eID, const _int iCurNumPlane);
	virtual _int		Update_GameObject(const _float& fTimeDelta);
	virtual _int		LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void		Render_GameObject();

private:
	EFF_PLAYER			m_eEffectID = EFF_PLAYER::EFF_END;
	_int				m_iNumPlane = 0;
	_uint				m_iNumTypePlane = 0;
	_float				m_fFrame_Speed;
	_float				m_fMove_Speed;
private:
	CTransform*			m_pTransformCom = nullptr;
	CTransform*			m_pTarget_TransCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CCollision*			m_pCollCom = nullptr;
private:
	_vec3				m_vTargetPos;
	_vec3				m_vRight, m_vUp;
private:
	HRESULT				Ready_Component();
	HRESULT				Ready_Texture();
public:
	static CEffect_Player* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
protected:
	virtual CGameObject * Clone_GameObject() override;
	virtual Engine::CGameObject * Clone_GameEffect(const Engine::_uint &iEffectID, const _int iCurNumPlane);
private:
	virtual void Free();

};

_END