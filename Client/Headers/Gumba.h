#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CCollision;
_END



_BEGIN(Client)
class CBuffer_Player;
class CObjMove;
class CPlane;
class CGumba final : public CGameObject
{
public:
	enum GUMBA_STATE { GUMBA_MOVE, GUMBA_DEAD, GUMBA_END};
	enum GUMBA_DIR { DIR_LEFT, DIR_RIGHT, DIR_END };

private:
	explicit CGumba(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CGumba(const CGumba& rhs);
	virtual ~CGumba() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();


	CObjMove* GetObjMove() { return m_pObjMove; }
public:
	static CGumba* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
	void	SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation);
public:
	virtual _bool GetRC(RC& rc);
private:
	HRESULT Ready_Component();
private:
	void AsGravity(const _float & fTImeDelta);
	void moveCheck();
public:
	virtual void Coll_Move(_vec3& vMove, _int i);


public:
	virtual _bool IsTurtle();
private:
	virtual void Free();
private:
	GUMBA_DIR		m_eDir = DIR_LEFT;
	_int			m_iNumPlane = 0;

private:
	CRenderer*		m_pRendererCom = nullptr;
	CCollision*		m_pCollCom = nullptr;
	CTransform*		m_pTransformCom = nullptr;
	CObjMove*		m_pObjMove = nullptr;
	CBuffer_Player*	m_pBufferCom = nullptr;
	CPlane*			m_pPlane = nullptr;
private:
	CTexture*		m_pTextureCom = nullptr;
	//_uint			m_iTexIdx = 0;
	_bool			m_bIsSound = false;
private:
	_int curPlane = 0;
	_int prePlane = 0;

	_bool m_bIsPos = false;

	Direction climbDir = Direction(0);//옥상으로 올라올때  올라온 면 판정때문에 필요
	Direction touchDir = Direction(0);

	_float offX = 0.f;
	_float offY = 0.f;

	_bool bCheck = false;

	_vec2 vSpd = { 0.f,0.f };
	_float ySpd = 2.f;//죽을때

	GUMBA_STATE eState = GUMBA_MOVE;
	_float		fDeadDelay = 0.f;

};

_END