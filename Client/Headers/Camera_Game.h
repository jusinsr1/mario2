#pragma once
#include "Camera.h"



_BEGIN(Client)
class CObjMove;
class CPlayer;
class CCamera_Game final : public CCamera
{
private:
	explicit CCamera_Game(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCamera_Game(const CCamera_Game& rhs);
	virtual ~CCamera_Game() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	void Set_PlayerMove(CObjMove* PlayerMove) { m_pPlayerMove = PlayerMove; }
	void Set_Player(CPlayer* Player) { m_pPlayer = Player; }

private:
	void Default_Camera();
	void CalCamera();
	void Fps_Camera();
	void Ending_Camera(const _float& fTimeDelta);
public:
	static CCamera_Game* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
protected:
	virtual void Free();

private:
	_uint m_iCameraState = 2;
	CObjMove* m_pPlayerMove = nullptr;
	CPlayer* m_pPlayer = nullptr;
	_int m_PlaneDir;
	_vec3 m_vCurCameraPos = { 200.f,30.f,200.f };
	_vec3 m_vCurLook = { 0.f,0.f,0.f };
	_vec3 m_vCurUp = { 0.f, 1.f, 0.f };
	_float m_fEndingTime = 0;
	Direction m_ClimbDir;
	_bool CameraChange = false;

};

_END