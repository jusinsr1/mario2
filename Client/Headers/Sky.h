#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_CubeTex;
_END

_BEGIN(Client)

class CSky final : public CGameObject
{
private:
	explicit CSky(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CSky(const CSky& rhs);
	virtual ~CSky() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CBuffer_CubeTex*		m_pBufferCom = nullptr;
private:
	HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
public:
	static CSky* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END