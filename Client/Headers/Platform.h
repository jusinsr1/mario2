#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CCollision;
_END

_BEGIN(Client)

class CPlatform final :
	public CGameObject
{
private:
	explicit CPlatform(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPlatform(const CPlatform& rhs);
	virtual ~CPlatform() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation);

	virtual void Coll_GameObject(_int colType);

public:
	virtual _bool GetRC(RC& rc);
private:
	CRenderer*				m_pRendererCom = nullptr;
	CBuffer_RcTex*			m_pBufferCom = nullptr;
	CTransform*				m_pTransformCom = nullptr;	
	CCollision*				m_pCollCom = nullptr;
private:
	CTexture*				m_pTextureCom = {};
private:
	_int					m_iNumPlane = -1;

	_int climeDir = 0;
private:
	HRESULT Ready_Component();
public:
	static CPlatform*			Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

};

_END