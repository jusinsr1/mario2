#pragma once
#include "GameObject.h"
#include "Input_Device.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CCollision;
_END

_BEGIN(Client)
class CBuffer_Player;
class CObjMove;
class CPlane;
class CPlayer final : public CGameObject
{
public:
	enum PLAYER { PLAYER_IDLE, PLAYER_WALK, PLAYER_DOWN, 
				  PLAYER_ATTACK,PLAYER_JUMP,PLAYER_END };

	enum PLAYER_DIR	{	DIR_LEFT,DIR_RIGHT , DIR_END};

public:
	PLAYER_DIR Get_Dir() { return m_eDir; }
	const _vec3 Get_Pos();
	_int		Get_PlaneNum() { return curPlane; }
	const _int	Get_Score() { return m_iScore; }
	const _int  Get_HitCnt() { return m_iCnt; }
private:
	explicit CPlayer(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPlayer(const CPlayer& rhs);
	virtual ~CPlayer() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();


	virtual void Coll_GameObject(_int colType);
	CObjMove* GetObjMove(){ return m_pObjMove; }
public:
	static CPlayer* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();

public:
	virtual _bool GetRC(RC& rc);
private:
	HRESULT Ready_Component();
	HRESULT	Ready_TextrueCom();
private:
	void KeyInput(const _float & fTImeDelta);
	void AsGravity(const _float & fTImeDelta);
	void moveCheck();


	void BossSound();
	void Fire(const _float & fTImeDelta);
	void Last();
public:
	virtual void Coll_Move(_vec3& vMove, _int i);
private:
	void Jump();
	void TireJump();
private:
	virtual void Free();
private:
	PLAYER			m_eCurState = PLAYER_IDLE;	PLAYER  m_ePreState = PLAYER_END;

	PLAYER_DIR		m_eDir = DIR_LEFT;
	
	enum CheckPoint {Zero =1,One = 6, Two = 12, Three = 17, Four = 21, Five = 27, Six = 31,Seven = 36,Eight = 41};
private:
	CInput_Device*  m_pInput_Device = nullptr;
	CSound_Manager*	m_pSoundManager = nullptr;
	CRenderer*		m_pRendererCom = nullptr;
	CCollision*		m_pCollCom = nullptr;
	CTransform*		m_pTransformCom = nullptr;
	CObjMove*		m_pObjMove = nullptr;
	CBuffer_Player*	m_pBufferCom = nullptr;
	CManagement*	m_pManageCom = nullptr;
	CPlane*			m_pPlane = nullptr;
private:
	CTexture*		m_pTextureCom[DIR_END] = {};
	//_uint			m_iTexIdx = 0;
	_uint			m_iScore = 0;
	_uint			m_iCnt = 0;
	_float			m_fHitTime = 0.f;
	_bool			m_bIsLast = false;
	_bool			m_bIsBgm = false;
private:
	_int curPlane = 0;
	_int prePlane = 0;

	Direction climbDir = Direction(0);//옥상으로 올라올때  올라온 면 판정때문에 필요
	Direction touchDir = Direction(0);

	_float offX = 0.f;
	_float offY = 0.f;

	_bool bCheck = false;


	_vec2 vSpd = {0.f,0.f};
	_float fSpd = 1.f;

	_uint cntJump = 0;


	_float fireDelay = 0.f;
};

_END