#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CCollision;
_END

_BEGIN(Client)
class CBuffer_Player;
class CObjMove;
class CPlane;
class CFire final : public CGameObject
{

private:
	explicit CFire(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CFire(const CFire& rhs);
	virtual ~CFire() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();


	CObjMove* GetObjMove() { return m_pObjMove; }
public:
	static CFire* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();

public:
	virtual _bool GetRC(RC& rc);
private:
	HRESULT Ready_Component();
private:
	void AsGravity(const _float & fTImeDelta);
	void moveCheck();

public:
	virtual void Coll_Move(_vec3& vMove, _int i);

private:
	virtual void Free();

private:
	CRenderer*		m_pRendererCom = nullptr;
	CCollision*		m_pCollCom = nullptr;
	CTransform*		m_pTransformCom = nullptr;
	CObjMove*		m_pObjMove = nullptr;
	CBuffer_Player*	m_pBufferCom = nullptr;
	CPlane*			m_pPlane = nullptr;
	CManagement*	m_pManageCom = nullptr;
	CTexture*		m_pTextureCom=nullptr;

public:
	void setData(_int curplane, CTransform* trs, _int dir,_int right);
private:
	_int curPlane = 0;
	_int prePlane = 0;

	Direction climbDir = Direction(0);//옥상으로 올라올때  올라온 면 판정때문에 필요
	Direction touchDir = Direction(0);

	_float offX = 0.f;
	_float offY = 0.f;

	_bool bCheck = false;


	_vec2 vSpd = { 0.f,0.f };


	_bool bDead = false;

};

_END