#pragma once

#include "GameObject.h"
#include "Input_Device.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CCollision;
_END

_BEGIN(Client)

class CAxe final :
	public CGameObject
{
public:
	_bool	GetLast() { return m_bIsLast; }

private:
	explicit CAxe(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CAxe(const CAxe& rhs);
	virtual ~CAxe() = default;


public:
	virtual void Coll_GameObject(_int colType);
	virtual _bool GetRC(RC& rc);
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	//public:
	//	virtual void		Coll_GameObject(_int colType);
	//	virtual _bool		GetRC(RC& rc);
private:
	CInput_Device*		m_pInput_Device = nullptr;
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CCollision*			m_pCollCom = nullptr;
private:
	_bool				m_bIsReady = false;
	_bool				m_bIsLast = false;
private:
	_float				fx = 0.f, fy = 0.f, fz = 0.f;
	_float				fcx = 0.f, fcy = 0.f, fcz = 0.f;
private:
	HRESULT				Ready_Component();

public:
	static CAxe*			Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

private:
	_int curPlane = 44;
	_int prePlane = 0;

	_bool m_bIsPos = false;

	Direction climbDir = Direction(0);//옥상으로 올라올때  올라온 면 판정때문에 필요
	Direction touchDir = Direction(0);

	_float offX = 0.f;
	_float offY = 0.f;

	_vec2 vSpd = { 0.f,0.f };
	_float ySpd = 2.f;//죽을때
};

_END