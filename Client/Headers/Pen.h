#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CCollision;
_END

_BEGIN(Client)

class CPen final 
	: public CGameObject
{
public:
	enum PEN { PEN_NONE, PEN_IDLE, PEN_MOVE, PEN_DEAD, PEN_END };
private:
	explicit CPen(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPen(const CPen& rhs);
	virtual ~CPen() = default;
public:
	virtual void		Coll_GameObject(_int colType);
	virtual _bool		GetRC(RC& rc);
public:
	virtual HRESULT		Ready_Prototype();
	virtual HRESULT		Ready_GameObject();
	virtual _int		Update_GameObject(const _float& fTimeDelta);
	virtual _int		LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void		Render_GameObject();
	virtual void		SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation, const _int& iVariable);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CCollision*			m_pCollCom = nullptr;
private:
	CTexture*			m_pTextureCom[PEN_END] = {};
private:
	PEN					m_eCurState = PEN_NONE;
	PEN					m_ePreState = PEN_END;
private:
	_float				m_fSpeed		= 0.f;
	_float				m_fTimeBegin	= 0.f;
	_float				m_fMyTimeBegin = 0.f;
	_int				m_iType	= 0;

private:
	_bool				m_bIsNone = false;

private:
	_int				m_iNumPlane = -1;

private:
	HRESULT	Ready_State();
	_int	Update_State(const _float& fTimeDelta);
private:
	HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
public:
	static CPen* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END