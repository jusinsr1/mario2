#pragma once


#include "Texture.h"

_BEGIN(Engine)
class CManagement;
_END
_BEGIN(Client)

class CLoad_Manager final: public CBase
{
	_DECLARE_SINGLETON(CLoad_Manager)
private:
	explicit CLoad_Manager();
	virtual ~CLoad_Manager() = default;
public:
	HRESULT Load_3DCube(const wstring & wstrPath, const _tchar* pLayerBuildingTag, const _tchar* pLayerTireTag, const _tchar* pLayerBoxTag);
	HRESULT Load_2DSquare(const wstring & wstrPath, const _tchar* pMonsterTag, const _tchar* pMapTag, const _tchar* pinteraction);
	HRESULT Load_PlaneData(const wstring& wstrPath);
	HRESULT Load_Texture(CTexture::TYPE eTypeID,const wstring& wstrPath);
	map<_uint, PLANEINFO*>* GetPlaneData() { return &m_mapPlaneData; }

private:
	map<_uint, PLANEINFO*> m_mapPlaneData;
	typedef map<_uint, PLANEINFO*> MAPPLANEDATA;
	vector<wstring*> m_vWstring;
	LPDIRECT3DDEVICE9 m_pGraphic_Device = nullptr;
	// CBase을(를) 통해 상속됨
	virtual void Free() override;

private:
};

_END