#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CCollision;
class CBuffer_RcTex;
_END

_BEGIN(Client)
class CObjMove;
class CPlane;

class CKoopa_Fire final : 
	public CGameObject
{
public:
	void SetPlane(_int iPlane) { curPlane = iPlane; }
	void SetPos(const _vec3 vPos) { m_vTarget = vPos; }

private:
	explicit CKoopa_Fire(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CKoopa_Fire(const CKoopa_Fire& rhs);
	virtual ~CKoopa_Fire() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();


	virtual void Coll_GameObject(_int colType);
	CObjMove* GetObjMove() { return m_pObjMove; }
public:
	static CKoopa_Fire* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();

private:
	HRESULT Ready_Component();
private:
	void AsGravity(const _float & fTImeDelta);
	void moveCheck();	
private:
	void Jump();
private:
	virtual void Free();
private:
	CRenderer*		m_pRendererCom = nullptr;
	CTransform*		m_pTransformCom = nullptr;
	CObjMove*		m_pObjMove = nullptr;
	CBuffer_RcTex*	m_pBufferCom = nullptr;
	CManagement*	m_pManageCom = nullptr;
	CPlane*			m_pPlane = nullptr;
private:
	CTexture*		m_pTextureCom = nullptr;

private:

	_vec3			m_vTarget;
	_bool			m_bIsLateInit = false;
private:
	_int curPlane = 0;
	_int prePlane = 0;

	Direction climbDir = Direction(0);//옥상으로 올라올때  올라온 면 판정때문에 필요
	Direction touchDir = Direction(0);

	_float offX = 0.f;
	_float offY = 0.f;

	_bool bCheck = false;


	_vec2 vSpd = { 0.f,0.f };
	_float fSpd = 1.f;

	_uint cntJump = 0;


	_float fireDelay = 0.f;
};

_END