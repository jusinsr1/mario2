#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
//class CRenderer;
class CCollision;
//class CBuffer_CubeLine;
_END

_BEGIN(Client)

class LastPang final : public CGameObject
{
private:
	explicit LastPang(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit LastPang(const LastPang& rhs);
	virtual ~LastPang() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();

public:
	virtual void GetCUBE(CB& cb);

public:
	static LastPang* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
private:
	HRESULT Ready_Component();

private:
	virtual void Free();

private:
	CTransform*			m_pTransformCom = nullptr;
	//CRenderer*			m_pRendererCom = nullptr;
	//CBuffer_CubeLine*	m_pBufferCom = nullptr;
	//CTexture*			m_pTextureCom = nullptr;
	CCollision*			m_pCollCom = nullptr;

	_float fSpd = 0.f;
};

_END