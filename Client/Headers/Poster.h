#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
_END

_BEGIN(Client)

class CPoster final :
	public CGameObject
{
public:
	enum POSTER_TYPE	{ POSTER_BIG , POSTER_SMALL , POSTER_END};

private:
	explicit CPoster(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPoster(const CPoster& rhs);
	virtual ~CPoster() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation, const _int& iVariable);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;

private:
	CTexture*			m_pTextureCom[POSTER_END] = {};

private:
	POSTER_TYPE			m_eType = POSTER_BIG;
private:
	HRESULT				Ready_Component();
public:
	static CPoster*			Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

};

_END