#include "stdafx.h"
#include "SaveLoad_Manager.h"
#include "Cube.h"
#include "Square.h"

IMPLEMENT_SINGLETON(CSaveLoad_Manager);

CSaveLoad_Manager::CSaveLoad_Manager()
{
}

CSaveLoad_Manager::~CSaveLoad_Manager()
{
	Release();
}

void CSaveLoad_Manager::Add3DCube(Object3D ID, CObj * CObj)
{
	CObj->SetType(ID);
	m_vec3D[ID].push_back(CObj);
}

void CSaveLoad_Manager::AddBaseTile(CObj* CObj)
{
	m_BaseTile = CObj;
}

void CSaveLoad_Manager::AddCamera(CCamera * CCamera)
{
	m_Camera = CCamera;
}

void CSaveLoad_Manager::AddSquare(Object2D ID, CObj * CObj)
{

	m_vec2D[ID].push_back(CObj);
}

void CSaveLoad_Manager::AddTexData(const wstring ImgTag, const wstring ImgPath)
{
	m_TextureData.insert({ ImgTag, ImgPath });
}

wstring CSaveLoad_Manager::Find_Path(const wstring & ImgTag)
{
	auto iter = m_TextureData.find(ImgTag);
	if (iter == m_TextureData.end())
		return wstring();
	return iter->second;
}

void CSaveLoad_Manager::Update()
{
	if (m_BaseTile != nullptr)
		m_BaseTile->Update();
	if (m_Camera != nullptr)
		m_Camera->Update();
	for (int i = 0; i<Object3D::_3DEND; i++)
	{
		int j = 0;
		for (auto& Object : m_vec3D[i])
		{
			if (Object == nullptr)
			{
				m_vec3D->erase(m_vec3D->begin() + j);
				continue;
			}
			Object->Update();
		}
	}

	for (int i = 0; i<Object2D::_2DEND; i++)
	{
		int j = 0;
		for (auto& Object : m_vec2D[i])
		{
			if (Object == nullptr)
			{
				m_vec2D->erase(m_vec2D->begin() + j);
				continue;
			}
			Object->Update();
		}
	}
}

_vec4 CSaveLoad_Manager::PickCheck(POINT mouse)
{
	_vec3	vMouseRay, vCameraPos;
	CalCulateRay(mouse,&vMouseRay, &vCameraPos);
	
	_vec4 Picking = { -1, -1, -1,-1 };
	_float dist = 0;

	CObj* PickObj = nullptr;
	Picking = m_BaseTile->UpdateRay(&vMouseRay, &vCameraPos, &dist);
	_float BuildNum = -1;
	for (_int i = 0; i<Object3D::_3DEND; i++)
	{
		_uint j = 0;
		for (auto& Object : m_vec3D[i])
		{
			if (Object == nullptr)
				return _vec4();
			_vec4 TempPicking = { -1,-1,-1, -1 };
			TempPicking = Object->UpdateRay(&vMouseRay, &vCameraPos, &dist);
			if (TempPicking.x != -1)
			{
				if (Object->GetType() == 0)
					BuildNum = j * 5 + TempPicking.w;
				PickObj = Object;
				m_Is2D3D = true;
				Picking = TempPicking;
			}
			j++;
		}
	}
	for (_int i = 0; i<Object2D::_2DEND; i++)
	{
		_uint j = 0;
		for (auto& Object : m_vec2D[i])
		{
			if (Object == nullptr)
				return _vec4();
			_vec4 TempPicking = { -1,-1,-1, -1 };
			TempPicking = Object->UpdateRay(&vMouseRay, &vCameraPos, &dist);
			if (TempPicking.x != -1)
			{
				BuildNum = TempPicking.w;
				PickObj = Object;
				m_Is2D3D = false;
				Picking = TempPicking;
			}
			j++;
		}
	}




	m_SelectObj = PickObj;
	return _vec4(Picking.x,Picking.y,Picking.z, BuildNum);
}

void CSaveLoad_Manager::CalCulateRay(POINT mouse, _vec3 * vMouseRay, _vec3 * vCameraPos)
{
	D3DVIEWPORT9		ViewPort;
	GET_INSTANCE(CDevice)->GetDevice()->GetViewport(&ViewPort);
	GET_INSTANCE(CKeyMgr)->IsMouseOnView(mouse);
	_vec4	vMousePos;
	vMousePos.x = mouse.x / (ViewPort.Width * 0.5f) - 1.f;
	vMousePos.y = mouse.y / ((ViewPort.Height * 0.5f) * -1.f) + 1.f;
	vMousePos.z = 0.0f;
	vMousePos.w = 1.f;

	_matrix	matProj;
	GET_INSTANCE(CDevice)->GetDevice()->GetTransform(D3DTS_PROJECTION, &matProj);
	D3DXMatrixInverse(&matProj, nullptr, &matProj);
	D3DXVec4Transform(&vMousePos, &vMousePos, &matProj);


	*vCameraPos = {0.f, 0.f, 0.f};
	*vMouseRay = _vec3(vMousePos.x, vMousePos.y, vMousePos.z) - *vCameraPos;

	_matrix	matView;
	GET_INSTANCE(CDevice)->GetDevice()->GetTransform(D3DTS_VIEW, &matView);

	D3DXMatrixInverse(&matView, nullptr, &matView);
	D3DXVec3TransformCoord(vCameraPos, vCameraPos, &matView);
	D3DXVec3TransformNormal(vMouseRay, vMouseRay, &matView);

}

void CSaveLoad_Manager::Render()
{
	if (m_BaseTile != nullptr)
		m_BaseTile->Render();
	if (m_Camera != nullptr)
		m_Camera->Render();
	for (int i = 0; i < Object3D::_3DEND; i++)
	{
		int j = 0;
		for (auto& Object : m_vec3D[i])
		{
			if (Object == nullptr)
			{
				m_vec3D->erase(m_vec3D->begin() + j);
				continue;
			}
			Object->Render();
		}
	}
	for (int i = 0; i < Object2D::_2DEND; i++)
	{
		int j = 0;
		for (auto& Object : m_vec2D[i])
		{
			if (Object == nullptr)
			{
				m_vec2D->erase(m_vec2D->begin() + j);
				continue;
			}
			Object->Render();
		}
	}
}

void CSaveLoad_Manager::Release()
{
	SafeDelete(m_BaseTile);
	SafeDelete(m_Camera);

	for (int i = 0; i < Object3D::_3DEND; i++)
	{
		for (auto& Object : m_vec3D[i])
			SafeDelete(Object);
		m_vec3D[i].clear();
	}
	for (int i = 0; i < Object2D::_2DEND; i++)
	{
		for (auto& Object : m_vec2D[i])
			SafeDelete(Object);
		m_vec2D[i].clear();
	}
}

void CSaveLoad_Manager::DeleteObj(CObj * Obj)
{
	for (int i = 0; i < Object3D::_3DEND; i++)
	{
		vector<CObj*>::iterator iter;
		for (iter = m_vec3D[i].begin(); iter != m_vec3D[i].end();)
		{
			if (*iter == Obj)
			{
				SafeDelete(Obj);
				iter = m_vec3D[i].erase(iter);
				return;
			}
			else
				iter++;
		}
	}
	for (int i = 0; i < Object2D::_2DEND; i++)
	{
		vector<CObj*>::iterator iter;
		for (iter = m_vec2D[i].begin(); iter != m_vec2D[i].end();)
		{
			if (*iter == Obj)
			{
				SafeDelete(Obj);
				iter = m_vec2D[i].erase(iter);
				return;
			}
			else
				iter++;
		}
	}
}

void CSaveLoad_Manager::SaveObject(const wstring &wstrPath, _bool State)
{
		wofstream fout;
		fout.open(wstrPath);
		if (fout.fail())
			return;
		if (State)
		{
			for (int i = 0; i < Object3D::_3DEND; i++)
			{
				for (auto& Object : m_vec3D[i])
				{
					if (Object != nullptr)
					{
						wstring SaveData = L"";
						SaveData = Object->Save3DObj();
						fout << SaveData << endl;
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < Object2D::_2DEND; i++)
			{
				for (auto& Object : m_vec2D[i])
				{
					if (Object != nullptr)
					{

						wstring SaveData = L"";
						SaveData = Object->Save2DObj();
						fout << SaveData << endl;
					}
				}
			}
		}
	fout.close();
}

void CSaveLoad_Manager::LoadObject(const wstring &wstrPath, _bool State)
{
	wifstream fin;

	fin.open(wstrPath);

	if (fin.fail())
		return;

	TCHAR szBuf[MAX_STR] = L"";
	

	while (true)
	{
		if (State == true)
		{
			_vec3 Position;
			_vec3 Scale;
			wstring wstrImgTag;
			wstring wstrImgPath;
			Object3D Type3D;
			fin.getline(szBuf, MAX_STR, '|');
			wstrImgTag = szBuf;

			wstrImgPath = Find_Path(wstrImgTag);

			fin.getline(szBuf, MAX_STR, '|');
			Position.x = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Position.y = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Position.z = (float)_wtof(szBuf);

			fin.getline(szBuf, MAX_STR, '|');
			Scale.x = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Scale.y = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Scale.z = (float)_wtof(szBuf);

			fin.getline(szBuf, MAX_STR);
			Type3D = (Object3D)_wtoi(szBuf);

			if (fin.eof())
				break;
			Add3DCube(Type3D, CCube::Create(GET_INSTANCE(CDevice)->GetDevice(), Position, Scale, wstrImgPath, wstrImgTag));
		}
		else
		{
			_uint i2Dtype;
			_uint iRotation;
			_int iVariable;
			_int iPlaneNum;
			_vec3 Position;
			_vec3 Scale = { 1,1,1 };
			Object2D Type2D = Normal2D;
			fin.getline(szBuf, MAX_STR, '|');
			i2Dtype = _wtoi(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			iRotation = _wtoi(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			iVariable = _wtoi(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			iPlaneNum = _wtoi(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Position.x = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Position.y = (float)_wtof(szBuf);
			
			if(i2Dtype == 9)
				fin.getline(szBuf, MAX_STR, '|');
			else
				fin.getline(szBuf, MAX_STR);
			Position.z = (float)_wtof(szBuf);
			
			if (i2Dtype == 9)
			{
				fin.getline(szBuf, MAX_STR, '|');
				Scale.x = (float)_wtof(szBuf);
				fin.getline(szBuf, MAX_STR, '|');
				Scale.y = (float)_wtof(szBuf);
				Type2D = CameraBox;
			}

			if (fin.eof())
				break;
			CObj* m_pObj = nullptr;
			AddSquare(Type2D, m_pObj = CSquare::Create(GET_INSTANCE(CDevice)->GetDevice(), Position, i2Dtype, iVariable));
			m_pObj->SetPlaneNum(iPlaneNum, iRotation);
			if (i2Dtype == 9)
				m_pObj->Set_Scale(Scale);
		}
	}

	fin.close();
}

void CSaveLoad_Manager::SetAllPlane(map<_int, PLANEINFO>* m_mapPlaneData)
{
	m_mapPlaneData->clear();
	int k = 0;
	for (int i = 0; i < Object3D::_3DEND; i++)
	{
		for (auto& Object : m_vec3D[i])
		{
			if (Object->GetType() == 0)
			{
				PLANEINFO PlaneData;
				for (size_t j = 0; j < 5; j++)
				{
					PlaneData = Object->Get_PlaneInfo(j, k);
					m_mapPlaneData->insert({ PlaneData.iPlaneNum, PlaneData });
				}
				k++;
			}
		}
	}
}