#pragma once

#include "Obj.h"
#include "Camera.h"

class CObj;
class CCamera;
class CSaveLoad_Manager final
{
	DECLARE_SINGLETON(CSaveLoad_Manager);
	

private:
	explicit CSaveLoad_Manager();
	virtual ~CSaveLoad_Manager();
public:
	void AddBaseTile(CObj* CObj);
	void AddCamera(CCamera* CObj);
	void Add3DCube(Object3D ID, CObj* CObj);
	void AddSquare(Object2D ID, CObj * CObj);
	void AddTexData(const wstring ImgTag, const wstring ImgPath);
	_vec4 PickCheck(POINT mouse);
	void CalCulateRay(POINT mouse, _vec3* vMouseRay, _vec3* vCameraRay);
	wstring Find_Path(const wstring& ImgTag);
	CObj* GetSelObj() {return m_SelectObj; }
	_bool IsSel2D3D() { return m_Is2D3D; }
	CCamera* GetCamera() { return m_Camera; }
	void Update();
	void Render();
	void Release();

	void DeleteObj(CObj* Obj);

	void SaveObject(const wstring &wstrPath, _bool State);
	void LoadObject(const wstring &wstrPath, _bool State);
	void SetAllPlane(map<_int, PLANEINFO>* m_mapPlaneData);
private:
	CObj* m_BaseTile = nullptr;
	CCamera* m_Camera = nullptr;

	vector<CObj*> m_vec3D[_3DEND];
	vector<CObj*> m_vec2D[_2DEND];
	map<wstring, wstring> m_TextureData;
	CObj* m_SelectObj = nullptr;
	_bool m_Is2D3D = false;
};

