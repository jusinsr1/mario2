#pragma once
class CObj
{
public:
	explicit CObj(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CObj();

public:
	virtual void Ready_Object();
	virtual void Update() PURE;
	virtual	void Render() PURE;
	virtual void Release() PURE;
	virtual _vec4 UpdateRay(const _vec3* vMouseRay, const _vec3* vCameraPos, _float* dist);

public:
	enum STATE { STATE_RIGHT, STATE_UP, STATE_LOOK, STATE_POSITION, STATE_END };

	const _vec3* Get_StateInfo(STATE eState);
	void Set_StateInfo(STATE eState, const _vec3* pInfo);
	void SetUp_RotationX(const _float & fRadian);
	void SetUp_RotationY(const _float & fRadian);
	void SetUp_RotationZ(const _float & fRadian);
	void Rotation_X(const _float& fTimeDelta);
	void Rotation_Y(const _float& fTimeDelta);
	void Rotation_Z(const _float& fTimeDelta);
	void Rotation_AxisRight(const _float& fTimeDelta);
	void Go_Straight();
	void Go_Left();
	void Go_Right();
	void Go_Up();
	void Go_Down();
	void BackWard();
	void Scaling(const _vec3 & Scale);
	_matrix Get_Matrix_Inverse() const;
	_vec3 Get_Scale();
	wstring Save2DObj();
	wstring Save3DObj();
	_int GetType() { return m_iType; }
	void SetType(_int Type) { m_iType = Type; }
	PLANEINFO Get_PlaneInfo(const _uint Planenum, const _uint BuildingNum);
	void SetPlaneNum(const _int& iPlaneNum,const _int& iRotationNum);
public:
	void Set_Scale(_vec3 Scale) { m_Scale = Scale; Scaling(m_Scale); }
	void Set_Position(_vec3 Position) {	m_Position = Position; Set_StateInfo(STATE_POSITION, &m_Position);}
	const wstring& Get_ImgTag() { return m_wstrImageTag; }
protected:
	_matrix						m_matWorld;
	_vec3						m_Position = { 0,0,0 };
	_vec3						m_Scale = { 1,1,1 };
	_int						m_iType = -1;
	_int						m_iVariable = -1;
	wstring						m_wstrImageTag;
	LPDIRECT3DDEVICE9			m_pGraphic_Device = nullptr;
	_int						m_iPlaneNum = -1;
	_int						m_iRotationNum = 0;
};