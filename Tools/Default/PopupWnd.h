#pragma once



// CPopupWnd 폼 뷰입니다.

class CMySheet;
class CPopupWnd : public CFormView
{
	DECLARE_DYNCREATE(CPopupWnd)

protected:
	CPopupWnd();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CPopupWnd();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_POPUPWND };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CMySheet* m_pMySheet;
	virtual void OnInitialUpdate();

};


